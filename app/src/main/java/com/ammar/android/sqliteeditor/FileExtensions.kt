package com.ammar.android.sqliteeditor

import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.util.Log

/**
 * Created by Ammar Githam on 23-05-2017.
 */
private val TAG = "FileExtensions"

fun Uri.getPath(context: Context): String? {
    if ("content".equals(this.scheme, true)) {
        val projection = arrayOf("_data")
        var cursor: Cursor? = null
        try {
            cursor = context.contentResolver.query(this, projection, null, null, null)
            val columnIndex = cursor.getColumnIndexOrThrow("_data")
            if (cursor.moveToFirst()) {
                return cursor.getString(columnIndex)
            }
        }
        catch (e: Exception) {
            Log.e(TAG, "Error in getting path", e)
        }
        finally {
            cursor?.close()
        }
    }
    else if ("file".equals(this.scheme, true)) {
        return this.path
    }
    return null
}
