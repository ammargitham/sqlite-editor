package com.ammar.android.sqliteeditor

import android.app.Application

/**
 * Created by Ammar Githam on 01/05/2017.
 */
internal class SqliteEditorApplication : Application()
