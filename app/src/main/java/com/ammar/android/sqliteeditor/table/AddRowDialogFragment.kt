package com.ammar.android.sqliteeditor.table

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Typeface
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatDialogFragment
import android.util.SparseArray
import android.view.LayoutInflater
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import com.ammar.android.sqliteeditor.R
import com.ammar.android.sqliteeditor.getTextWatcherObservable
import com.ammar.android.sqliteeditor.utils.SerializableSparseArray
import com.ammar.android.sqliteeditor.utils.SerializableSparseBooleanArray
import com.ammar.android.sqliteeditor.utils.Utils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Function
import io.reactivex.subjects.PublishSubject
import java.util.*

/**
 * Created by Ammar Githam on 15-05-2017.
 */
internal class AddRowDialogFragment : AppCompatDialogFragment() {
    //private val TAG = "AddRowDialogFragment"
    private val mDisposables = CompositeDisposable()
    private var positiveButton: Button? = null
    private var buttonEnabled: Boolean = false
    private var mOnAddClick: (SparseArray<Any>) -> Unit? = {}

    @Suppress("UNCHECKED_CAST")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val columnNames = arguments.getSerializable("columnNames") as SerializableSparseArray<String>
        val primaryKeyColumnNames = arguments.getSerializable("primaryKeyColumnNames") as SerializableSparseArray<String>
        val columnTypes = arguments.getSerializable("columnTypes") as SerializableSparseArray<String>
        val columnDefaultValues = arguments.getSerializable("columnDefaultValues") as SerializableSparseArray<String>
        val columnNonNulls = arguments.getSerializable("columnNonNulls") as SerializableSparseBooleanArray
        val isAutoIncrement = arguments.getBoolean("isAutoIncrement")
        if (savedInstanceState != null) {
            buttonEnabled = savedInstanceState.getBoolean("buttonEnabled")
        }
        @SuppressLint("InflateParams")
        val rootView = LayoutInflater.from(context).inflate(R.layout.dialog_add_table_row, null)
        val addRowFormContainer = rootView.findViewById<LinearLayout>(R.id.form_container)
        val fieldObservables = ArrayList<Observable<Boolean>>(columnNames.size())
        val fieldSubjects = SparseArray<PublishSubject<String>>(columnNames.size())
        val fieldValues = SparseArray<String>(columnNames.size())
        val fields = arrayOfNulls<EditText>(columnNames.size())
        for (i in 0..columnNames.size() - 1) {
            val colNo = columnNames.keyAt(i)
            val columnName = columnNames.get(colNo)
            val columnType = columnTypes.get(colNo)
            var defaultValue: String? = columnDefaultValues.get(colNo)
            if (defaultValue != null && defaultValue.matches("^'.*'$".toRegex())) {
                defaultValue = defaultValue.substring(1, defaultValue.length - 1)
            }
            val isNotNull = columnNonNulls.get(colNo)
            var isPk = false
            for (j in 0..primaryKeyColumnNames.size() - 1) {
                isPk = primaryKeyColumnNames.valueAt(j) == columnName
                if (isPk) {
                    break
                }
            }
            val inputLayout = LayoutInflater.from(context)
                    .inflate(R.layout.content_add_row_input_template, addRowFormContainer, false) as TextInputLayout
            val inputEditText = inputLayout.findViewById<EditText>(R.id.add_row_template_edit_text)
            // setting id to properly restore view state
            inputEditText.id = i
            if (isPk && isAutoIncrement) {
                inputEditText.setText(R.string.auto_increment)
                inputEditText.setTag(R.id.autoincrement, true)
                inputEditText.setTypeface(inputEditText.typeface, Typeface.ITALIC)
                inputEditText.isEnabled = false
            }
            else if (defaultValue != null) {
                inputEditText.setText(defaultValue)
            }
            val format = "%s [%s]" + (if (isPk) " [pk]" else "") + if (isNotNull) " *" else ""
            inputLayout.hint = String.format(format, columnName, columnType)
            addRowFormContainer.addView(inputLayout)
            val textWatcherObservable = inputEditText.getTextWatcherObservable() as PublishSubject<String>
            val fieldObservable = textWatcherObservable
                    .observeOn(AndroidSchedulers.mainThread())
                    .map(Function<String, Boolean> { input ->
                        if (isNotNull && input.isEmpty()) {
                            inputEditText.error = columnName + " cannot be NULL"
                            return@Function false
                        }
                        true
                    })
            fieldObservables.add(fieldObservable)
            // Collecting the observables which already have text or are nullable
            // to trigger the Observable.combineLatest later
            if (!isNotNull || !inputEditText.text.toString().isEmpty()) {
                fieldSubjects.put(colNo, textWatcherObservable)
                fieldValues.put(colNo, inputEditText.text.toString())
            }
            fields[i] = inputEditText
        }
        mDisposables.add(Observable.combineLatest(fieldObservables) { objects ->
            var result = false
            for (`object` in objects) {
                result = `object` as Boolean
                if (!result) {
                    break
                }
            }
            result
        }.subscribe { o ->
            buttonEnabled = o as Boolean
            positiveButton?.isEnabled = buttonEnabled
        })
        val dialog = AlertDialog.Builder(activity)
                .setView(rootView)
                .setPositiveButton(R.string.alert_dialog_add) { _, _ ->
                    val onAddClick = mOnAddClick
                    val values = SparseArray<Any>(columnNames.size())
                    for (i in fields.indices) {
                        val editText = fields[i]
                        val inputText = editText?.text.toString()
                        val isNumeric = Utils.isNumeric(inputText)
                        if (isNumeric) {
                            if (inputText.contains("."))
                                values.put(i, java.lang.Float.valueOf(inputText)) // Float
                            else values.put(i, Integer.valueOf(inputText)) // integer
                        }
                        else values.put(i, inputText)// String
                    }
                    onAddClick(values)
                }
                .setNegativeButton(getString(R.string.alert_dialog_cancel)) { _, _ -> }
                .create()
        dialog.setOnShowListener { dialog ->
            positiveButton = (dialog as AlertDialog).getButton(DialogInterface.BUTTON_POSITIVE)
            positiveButton?.isEnabled = buttonEnabled
            // Emitting the already input text to the textWatcherObservables to trigger Observable.combineLatest
            (0..fieldSubjects.size() - 1)
                    .map { fieldSubjects.keyAt(it) }
                    .forEach { fieldSubjects.get(it).onNext(fieldValues.get(it)) }
        }
        return dialog
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean("buttonEnabled", buttonEnabled)
    }

    companion object {
        internal fun newInstance(columnNames: SerializableSparseArray<String>, primaryKeyColumnNames: SerializableSparseArray<String>,
                                 columnTypes: SerializableSparseArray<String>, columnDefaultValues: SerializableSparseArray<String>,
                                 columnNonNulls: SerializableSparseBooleanArray, isAutoIncrement: Boolean): AddRowDialogFragment {
            val fragment = AddRowDialogFragment()
            val args = Bundle()
            args.putSerializable("columnNames", columnNames)
            args.putSerializable("primaryKeyColumnNames", primaryKeyColumnNames)
            args.putSerializable("columnTypes", columnTypes)
            args.putSerializable("columnDefaultValues", columnDefaultValues)
            args.putSerializable("columnNonNulls", columnNonNulls)
            args.putBoolean("isAutoIncrement", isAutoIncrement)
            fragment.arguments = args
            return fragment
        }
    }

    fun onAddClick(listener: (SparseArray<Any>) -> Unit) {
        mOnAddClick = listener
    }
}