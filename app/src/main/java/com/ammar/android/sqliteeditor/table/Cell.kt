package com.ammar.android.sqliteeditor.table

internal data class Cell(var text: String? = "", var typeString: String? = "", val type: Int, val dataType: Int) {
    companion object {
        @JvmField val TYPE_HEADER = 0
        @JvmField val TYPE_NORMAL = 1
    }
}
