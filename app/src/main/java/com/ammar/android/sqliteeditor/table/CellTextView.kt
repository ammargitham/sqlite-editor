package com.ammar.android.sqliteeditor.table

import android.content.Context
import android.graphics.PorterDuff.Mode
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.widget.Checkable

internal class CellTextView : android.support.v7.widget.AppCompatTextView, Checkable {
    //private val TAG = "CellTextView"
    private val CHECKED_STATE_SET = intArrayOf(android.R.attr.state_checked)
    private var mChecked = false

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    fun setDrawableStates(stateSet: IntArray) {
        val compoundDrawables = compoundDrawables
        textColors?.let { setDrawables(it.getColorForState(stateSet, it.defaultColor), *compoundDrawables) }
    }

    fun setDrawables(color: Int, vararg drawables: Drawable?) {
        val porterDuffColorFilter = PorterDuffColorFilter(color, Mode.SRC_IN)
        for (drawable in drawables) {
            drawable?.mutate()?.colorFilter = porterDuffColorFilter
        }
    }

    override fun onCreateDrawableState(i: Int): IntArray {
        val drawableState = super.onCreateDrawableState(i + 1)
        setDrawableStates(drawableState)
        if (isChecked) {
            mergeDrawableStates(drawableState, CHECKED_STATE_SET)
        }
        return drawableState
    }

    override fun isChecked() = mChecked

    override fun setChecked(b: Boolean) {
        if (b != mChecked) {
            mChecked = b
            refreshDrawableState()
        }
    }

    override fun toggle() {
        isChecked = !mChecked
    }
}
