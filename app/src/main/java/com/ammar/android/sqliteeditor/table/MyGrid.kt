package com.ammar.android.sqliteeditor.table

import android.content.Context
import android.database.Cursor
import android.util.SparseArray
import com.ammar.android.sqliteeditor.R
import com.ammar.android.sqliteeditor.tableview.Column
import com.ammar.android.sqliteeditor.tableview.Grid
import com.ammar.android.sqliteeditor.tableview.Row
import com.ammar.android.sqliteeditor.utils.SqliteHelper
import com.ammar.android.sqliteeditor.utils.Utils
import java.util.*

internal class MyGrid(context: Context, cursor: Cursor, columnNames: SparseArray<String>, columnTypes: SparseArray<String>) : Grid<Cell>() {
    private val TAG = "MyGrid"
    private val rowHeight = Utils.dp2px(context, context.resources.getDimension(R.dimen.cell_height))
    private val columnWidth = Utils.dp2px(context, context.resources.getDimension(R.dimen.cell_width)).toInt()

    init {
        setGridHeaders(getColumnHeaders(columnNames, columnTypes, cursor.count), getRowHeaders(cursor.count), (rowHeight * 1.5f).toInt())
        setCellItems(cursor)
    }

    private fun setCellItems(cursor: Cursor) {
        cursor.moveToPosition(-1)
        var row = 1
        while (cursor.moveToNext()) {
            for (col in 0..cursor.columnCount - 1) {
                val text = SqliteHelper.getDataString(cursor, col)
                setItem(col + 1, row, Cell(text, null, Cell.TYPE_NORMAL, cursor.getType(col)))
            }
            row++
        }
    }

    private fun getRowHeaders(rowCount: Int): List<Row<Cell>> {
        val rows = ArrayList<Row<Cell>>()
        for (i in 1..rowCount) {
            val position = i.toString()
            val rowRow = Row(Cell(position, null, Cell.TYPE_HEADER, Cursor.FIELD_TYPE_STRING), position)
            rowRow.height = rowHeight.toInt()
            rows.add(rowRow)
        }
        return rows
    }

    private fun getColumnHeaders(columnNames: SparseArray<String>, columnTypes: SparseArray<String>, rowCount: Int): List<Column<Cell>> {

        val columns = ArrayList<Column<Cell>>()
        val topLeftCorner = Column(Cell("\u2116", rowCount.toString(), Cell.TYPE_HEADER, Cursor.FIELD_TYPE_STRING), "pos")
        topLeftCorner.columnWidth = columnWidth / 2
        columns.add(topLeftCorner)
        var columnNo = 0
        while (columnNo < columnNames.size()) {
            val column = Column(Cell(columnNames.get(columnNo), columnTypes.get(columnNo), Cell.TYPE_HEADER, Cursor.FIELD_TYPE_STRING),
                                columnNames.get(columnNo))
            //Log.d(TAG, "getColumnHeaders: column: " + column);
            column.setWidthAndWeight(columnWidth, 1.0f)
            columns.add(column)
            columnNo++
        }
        return columns
    }
}
