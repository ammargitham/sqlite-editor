package com.ammar.android.sqliteeditor.table

import android.database.Cursor
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.ammar.android.sqliteeditor.R
import com.ammar.android.sqliteeditor.tableview.Grid
import com.ammar.android.sqliteeditor.tableview.TableAdapter

internal class MyTableAdapter(grid: Grid<Cell>, onTableCellClickListener: TableAdapter.OnTableCellClickListener) : TableAdapter<Cell>(grid, onTableCellClickListener) {
    private val TAG = "MyTableAdapter"
    private var mLastSelectedView: CellTextView? = null
    private var mLastSelectedRow: Int = 0
    private var mLastSelectedColumn: Int = 0

    private inner class CellViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = if (viewType == Cell.TYPE_NORMAL)
            LayoutInflater.from(parent.context).inflate(R.layout.content_table_cell, parent, false) as CellTextView
        else
            LayoutInflater.from(parent.context).inflate(R.layout.content_table_header_cell, parent, false) as CellTextView
        view.gravity = Gravity.CENTER
        when (viewType) {
            Cell.TYPE_HEADER -> {
                view.setBackgroundColor(ContextCompat.getColor(parent.context, android.R.color.darker_gray))
                view.setTextColor(ContextCompat.getColor(parent.context, android.R.color.white))
                view.setPadding(0, 0, 1, 1)
            }
            Cell.TYPE_NORMAL -> view.setBackgroundColor(ContextCompat.getColor(parent.context, android.R.color.white))
        }
        return CellViewHolder(view)
    }

    override fun onBindViewHolder(row: Int, column: Int, view: View?, cell: Cell?) {
        if (cell == null || view == null) {
            return
        }
        val textView = view as CellTextView
        if (mLastSelectedColumn == column && mLastSelectedRow == row) {
            textView.isChecked = true
            mLastSelectedView = textView
        }
        if (cell.typeString != null) {
            textView.text = String.format("%s\n(%s)", cell.text, cell.typeString)
        }
        else {
            textView.text = cell.text
        }
        if (row != 0 && column != 0) {
            when (cell.dataType) {
                Cursor.FIELD_TYPE_FLOAT, Cursor.FIELD_TYPE_INTEGER -> textView.gravity = Gravity.RIGHT or Gravity.CENTER_VERTICAL
                Cursor.FIELD_TYPE_STRING -> textView.gravity = Gravity.LEFT or Gravity.CENTER_VERTICAL
            }
        }
    }

    override fun onViewRecycled(viewHolder: RecyclerView.ViewHolder) {
        val textView = viewHolder.itemView as CellTextView
        textView.text = ""
        textView.isChecked = false
    }

    override fun isClickable(row: Int, column: Int, cell: Cell) = column > 0

    override fun getItemViewType(row: Int, column: Int, cell: Cell) = cell.type

    fun selectCell(row: Int, column: Int, view: View) {
        if (mLastSelectedRow == row && mLastSelectedColumn == column) {
            return
        }
        mLastSelectedView?.isChecked = false
        val cellTextView = view as CellTextView
        cellTextView.isChecked = true
        mLastSelectedView = cellTextView
        mLastSelectedColumn = column
        mLastSelectedRow = row
    }

    fun clearSelected() {
        mLastSelectedView?.isChecked = false
        mLastSelectedView = null
        mLastSelectedRow = -1
        mLastSelectedColumn = -1
    }
}
