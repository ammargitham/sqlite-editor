package com.ammar.android.sqliteeditor.table

import android.content.ContentValues
import android.database.Cursor
import android.database.DatabaseErrorHandler
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.view.ActionMode
import android.support.v7.widget.Toolbar
import android.text.InputType
import android.util.Log
import android.util.SparseArray
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.ImageButton
import com.ammar.android.sqliteeditor.R
import com.ammar.android.sqliteeditor.tableview.TableAdapter
import com.ammar.android.sqliteeditor.tableview.TableDecorator
import com.ammar.android.sqliteeditor.tableview.TableRecyclerView
import com.ammar.android.sqliteeditor.utils.SerializableSparseArray
import com.ammar.android.sqliteeditor.utils.SerializableSparseBooleanArray
import com.ammar.android.sqliteeditor.utils.SqliteHelper
import com.ammar.android.sqliteeditor.utils.Utils
import com.stericson.RootTools.RootTools
import java.io.File

internal class TableActivity : AppCompatActivity() {
    private var mTableName: String? = null
    private var mOriginalDbFilePath: String? = null
    private var mIsOriginalPathSystem: Boolean = false
    private var mTableView: TableRecyclerView? = null
    private var mEditText: EditText? = null
    private var mSingleChoiceModeCallback: ActionMode.Callback? = null
    private var mChoiceActionMode: ActionMode? = null
    private var mCommitButton: ImageButton? = null
    private var mDbFile: File? = null
    private var mColumnTypes: SerializableSparseArray<String>? = null
    private var mColumnNonNulls: SerializableSparseBooleanArray? = null
    private var mColumnDefaultValues: SerializableSparseArray<String>? = null
    private var mPrimaryKeyColumnNames: SerializableSparseArray<String>? = null
    private var mColumnNames: SerializableSparseArray<String>? = null
    private var mTableSchema: String? = null
    private var mSelectedColumn: Int = 0
    private var mSelectedRow: Int = 0
    private var mGrid: MyGrid? = null
    private var mSelectedView: View? = null
    private var mSelectedCell: Cell? = null
    private var mTableAdapter: MyTableAdapter? = null
    private var mPkIsAutoInc: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_table)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        val dbFilePath = intent.getStringExtra(EXTRA_DB_FILE_PATH)
        mTableName = intent.getStringExtra(EXTRA_TABLE_NAME)
        mOriginalDbFilePath = intent.getStringExtra(EXTRA_ORIGINAL_FILE_PATH)
        mIsOriginalPathSystem = intent.getBooleanExtra(EXTRA_IS_ORIGINAL_PATH_SYSTEM, false)
        if (dbFilePath == null || mTableName == null) {
            return
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = mTableName
        mEditText = findViewById(R.id.input_edit_text)
        mTableView = findViewById<TableRecyclerView>(R.id.table)
        mTableView?.tableDecorator = TableDecorator.Builder()
                .setVerticalDivider(ContextCompat.getColor(this, android.R.color.black), 1)
                .setHorizontalDivider(ContextCompat.getColor(this, android.R.color.black), 1)
                .build()
        mSingleChoiceModeCallback = object : ActionMode.Callback {
            override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
                mode.menuInflater.inflate(R.menu.action_mode_cell_selection, menu)
                return true
            }

            override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
                return true
            }

            override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
                return false
            }

            override fun onDestroyActionMode(mode: ActionMode) {
                clearSelection()
            }
        }
        mCommitButton = findViewById(R.id.commit)
        mCommitButton?.setOnClickListener { commitCellValue() }
        mDbFile = File(dbFilePath)
        getTableInfo()
        populate()
    }

    private fun getTableInfo() {
        var db: SQLiteDatabase? = null
        var cursor: Cursor? = null
        try {
            db = SQLiteDatabase.openDatabase(mDbFile?.absolutePath, null, SQLiteDatabase.OPEN_READWRITE,
                    DatabaseErrorHandler { Log.e(TAG, "onCorruption") })
            cursor = db.rawQuery("pragma table_info($mTableName)", null)
            //Log.d(TAG, "getTableInfo: tableNames: " + Arrays.toString(cursor.getColumnNames()));
            // Columns in order: cid, name, type, notnull, dflt_value, pk
            mColumnNames = SerializableSparseArray<String>(cursor.count)
            mColumnTypes = SerializableSparseArray<String>(cursor.count)
            mColumnNonNulls = SerializableSparseBooleanArray(cursor.count)
            mColumnDefaultValues = SerializableSparseArray<String>(cursor.count)
            mPrimaryKeyColumnNames = SerializableSparseArray<String>(1)
            var colNum = 0
            while (cursor.moveToNext()) {
                mColumnNames?.put(colNum, cursor.getString(1))
                mColumnTypes?.put(colNum, cursor.getString(2))
                mColumnNonNulls?.put(colNum, cursor.getInt(3) == 1)
                mColumnDefaultValues?.put(colNum, SqliteHelper.getDataString(cursor, 4))
                if (cursor.getInt(5) == 1) {
                    mPrimaryKeyColumnNames?.put(colNum, cursor.getString(1))
                }
                colNum++
            }
            cursor.close()
            cursor = db.rawQuery("SELECT sql FROM sqlite_master WHERE name='$mTableName'", null)
            cursor.moveToFirst()
            mTableSchema = cursor.getString(0)
            if (Utils.containsIgnoreCase(mTableSchema, "AUTOINCREMENT")) {
                mPkIsAutoInc = true
            }
        }
        catch (e: SQLiteException) {
            Log.e(TAG, "getTableInfo: ", e)
        }
        finally {
            cursor?.close()
            db?.close()
        }
    }

    private fun populate() {
        var db: SQLiteDatabase? = null
        var cursor: Cursor? = null
        try {
            db = SQLiteDatabase.openDatabase(mDbFile?.absolutePath, null, SQLiteDatabase.OPEN_READONLY,
                    DatabaseErrorHandler { Log.e(TAG, "Corrupted database") })
            cursor = db.query(mTableName, null, null, null, null, null, null)
            val columnNames = mColumnNames ?: return
            val columnTypes = mColumnTypes ?: return
            mGrid = MyGrid(this, cursor, columnNames, columnTypes)
            val grid = mGrid ?: return
            mTableAdapter = MyTableAdapter(grid, TableAdapter.OnTableCellClickListener { x, y, view, obj ->
                startSupportActionModeIfNeeded()
                mSelectedColumn = x
                mSelectedRow = y
                mSelectedView = view
                mTableAdapter?.selectCell(y, x, view)
                mSelectedCell = obj as Cell
                val editText = mEditText ?: return@OnTableCellClickListener
                if (!editText.isEnabled) {
                    editText.apply {
                        isEnabled = true
                        isFocusable = true
                        isFocusableInTouchMode = true
                    }
                    mCommitButton?.visibility = View.VISIBLE
                }
                editText.setText(mSelectedCell?.text)
                when (mSelectedCell?.dataType) {
                    Cursor.FIELD_TYPE_FLOAT -> editText.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
                    Cursor.FIELD_TYPE_INTEGER -> editText.inputType = InputType.TYPE_CLASS_NUMBER
                }
                editText.selectAll()
            })
            mTableView?.tableAdapter = mTableAdapter
        }
        catch (e: SQLiteException) {
            Log.e(TAG, "populate: ", e)
        }
        finally {
            cursor?.close()
            db?.close()
        }
    }

    private fun commitCellValue() {
        var db: SQLiteDatabase? = null
        try {
            db = SQLiteDatabase.openDatabase(mDbFile?.absolutePath, null, SQLiteDatabase.OPEN_READWRITE,
                    DatabaseErrorHandler { Log.e(TAG, "Corrupted database") })
            var where = ""
            val primaryKeyColumnNames = mPrimaryKeyColumnNames ?: return
            val whereArgs = arrayOfNulls<String>(primaryKeyColumnNames.size())
            for (i in 0..primaryKeyColumnNames.size() - 1) {
                if (i != 0) {
                    where += " AND "
                }
                val gridColNo = primaryKeyColumnNames.keyAt(i) + 1
                val tableName = primaryKeyColumnNames.valueAt(i)
                whereArgs[i] = mGrid?.getItem(gridColNo, mSelectedRow)?.text
                where += tableName + " = ?"
            }
            val contentValues = ContentValues()
            val updatedColName = mColumnNames?.get(mSelectedColumn - 1)
            contentValues.put(updatedColName, mEditText?.text.toString())
            val rowsUpdated = db.update(mTableName, contentValues, where, whereArgs)
            if (rowsUpdated > 0) {
                mSelectedCell?.text = mEditText?.text.toString()
                (mSelectedView as CellTextView).text = mEditText?.text.toString()
                mGrid?.setItem(mSelectedColumn, mSelectedRow, mSelectedCell)
                mTableAdapter?.notifyDataSetChanged()
            }
            clearSelection()
        }
        catch (e: SQLiteException) {
            Log.e(TAG, "populate: ", e)
        }
        finally {
            db?.close()
            if (mIsOriginalPathSystem && mOriginalDbFilePath != mDbFile?.absolutePath) {
                copyFileToOriginalPath()
            }
        }
    }

    private fun startSupportActionModeIfNeeded() {
        if (mChoiceActionMode == null) {
            val singleChoiceModeCallback = mSingleChoiceModeCallback ?: throw IllegalStateException("No callback set")
            mChoiceActionMode = startSupportActionMode(singleChoiceModeCallback)
        }
    }

    override fun onBackPressed() {
        if (mChoiceActionMode != null) {
            clearSelection()
            return
        }
        super.onBackPressed()
    }

    private fun clearSelection() {
        mTableAdapter?.clearSelected()
        mSelectedColumn = -1
        mSelectedRow = -1
        mEditText?.setText(getString(R.string.input_unselected_hint))
        mEditText?.isEnabled = false
        mEditText?.isFocusable = false
        mEditText?.isFocusableInTouchMode = false
        mCommitButton?.visibility = View.GONE
        mChoiceActionMode?.finish()
        mChoiceActionMode = null
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_table, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.table_add_row) {
            showAddRowDialog()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showAddRowDialog() {
        val columnNames = mColumnNames ?: return
        val primaryKeyColumnNames = mPrimaryKeyColumnNames ?: return
        val columnTypes = mColumnTypes ?: return
        val columnDefaultValues = mColumnDefaultValues ?: return
        val columnNonNulls = mColumnNonNulls ?: return
        val addRowDialogFragment = AddRowDialogFragment.newInstance(columnNames, primaryKeyColumnNames, columnTypes,
                columnDefaultValues, columnNonNulls, mPkIsAutoInc)
        addRowDialogFragment.onAddClick { values -> addRow(values) }
        addRowDialogFragment.show(supportFragmentManager, "AddRowDialogFragment")
    }

    private fun addRow(values: SparseArray<Any>) {
        var db: SQLiteDatabase? = null
        var rowId: Long = -1
        try {
            db = SQLiteDatabase.openDatabase(mDbFile?.absolutePath, null, SQLiteDatabase.OPEN_READWRITE,
                    DatabaseErrorHandler { Log.e(TAG, "Corrupted database") })
            val contentValues = ContentValues()
            for (i in 0..values.size() - 1) {
                val columnName = mColumnNames?.valueAt(i)
                var isPk = false
                val primaryKeyColumnNames = mPrimaryKeyColumnNames ?: return
                for (j in 0..primaryKeyColumnNames.size() - 1) {
                    isPk = primaryKeyColumnNames.valueAt(j) == columnName
                    if (isPk) {
                        break
                    }
                }
                if (isPk && mPkIsAutoInc) {
                    contentValues.put(columnName, null as String?)
                    continue
                }
                val valueObj = values.valueAt(i)
                if (valueObj is Float) {
                    contentValues.put(columnName, valueObj)
                }
                else if (valueObj is Int) {
                    contentValues.put(columnName, valueObj)
                }
                else {
                    var value: String? = valueObj as String
                    // Make empty strings null
                    if (value != null && value.isEmpty()) {
                        value = null
                    }
                    contentValues.put(columnName, value)
                }
            }
            rowId = db.insertOrThrow(mTableName, null, contentValues)
            clearSelection()
        }
        catch (e: SQLiteException) {
            Log.e(TAG, "addRow: ", e)
        }
        finally {
            db?.close()
            if (rowId > 0) {
                populate()
                mTableView?.scrollToRow(rowId.toInt())
                if (mIsOriginalPathSystem && mOriginalDbFilePath != mDbFile?.absolutePath) {
                    copyFileToOriginalPath()
                }
            }
        }
    }

    private fun copyFileToOriginalPath() {
        RootTools.copyFile(mDbFile?.absolutePath, mOriginalDbFilePath, true, false)
    }

    companion object {
        private val TAG = "TableActivity"
        val EXTRA_TABLE_NAME = "table_name"
        val EXTRA_DB_FILE_PATH = "db_file_path"
        val EXTRA_ORIGINAL_FILE_PATH = "original_file_path"
        val EXTRA_IS_ORIGINAL_PATH_SYSTEM = "is_og_path_system"
    }
}
