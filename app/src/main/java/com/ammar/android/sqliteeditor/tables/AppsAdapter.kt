package com.ammar.android.sqliteeditor.tables

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.ammar.android.sqliteeditor.R

/**
 * Created by Ammar Githam on 11-05-2017.
 */
internal class AppsAdapter(private val apps: List<AppsDbChooserActivity.AppInfo>) : RecyclerView.Adapter<AppsAdapter.ViewHolder>() {
    private var mOnItemClick: (position: Int, appInfo: AppsDbChooserActivity.AppInfo?, v: View) -> Unit = { _, _, _ -> }

    internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        internal val iconView = itemView.findViewById<ImageView>(R.id.app_icon)
        internal val appName = itemView.findViewById<TextView>(R.id.app_name)
        internal var appInfo: AppsDbChooserActivity.AppInfo? = null

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            mOnItemClick(adapterPosition, appInfo, v)
        }
    }

    fun onItemClick(listener: (position: Int, appInfo: AppsDbChooserActivity.AppInfo?, v: View) -> Unit) {
        mOnItemClick = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppsAdapter.ViewHolder {
        val tableNameView = LayoutInflater.from(parent.context).inflate(R.layout.content_app_item, parent, false)
        return ViewHolder(tableNameView)
    }

    override fun onBindViewHolder(holder: AppsAdapter.ViewHolder, position: Int) {
        val appInfo = apps[position]
        holder.iconView.setImageDrawable(appInfo.icon)
        holder.appName.text = appInfo.name
        holder.appInfo = appInfo
    }

    override fun getItemCount(): Int {
        return apps.size
    }
}
