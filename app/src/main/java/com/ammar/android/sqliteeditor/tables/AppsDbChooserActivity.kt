package com.ammar.android.sqliteeditor.tables

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.database.DatabaseErrorHandler
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import com.ammar.android.sqliteeditor.R
import com.stericson.RootTools.RootTools
import eu.chainfire.libsuperuser.Shell
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import java.io.File
import java.util.*

internal class AppsDbChooserActivity : AppCompatActivity() {

    private val TAG = "AppsDbChooserActivity"
    private var mDisposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_apps_db_chooser)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Select from Apps"
        val appsRecyclerView = findViewById<RecyclerView>(R.id.apps)
        appsRecyclerView ?: return
        val apps = ArrayList<AppInfo>()
        val appsAdapter = AppsAdapter(apps)
        appsAdapter.onItemClick { position, appInfo, v ->
            Log.d(TAG, "position: $position appInfo: $appInfo v: $v")
            appInfo ?: return@onItemClick
            val dialog = ProgressDialogFragment.newInstance("Reading database files...")
            val dbs = mutableListOf<DbInfo>()
            mDisposables.add(
                    Single.just(appInfo)
                            .filter { appInfo -> checkDatabaseFolderExists(appInfo) }
                            .flatMapSingle { appInfo -> getDatabasesForApp(appInfo) }
                            .onErrorReturn { mutableListOf() }
                            .flatMapObservable { dbs -> Observable.fromIterable(dbs) }
                            .filter { dbInfo -> checkDb(dbInfo) }
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .doOnSubscribe { dialog.show(supportFragmentManager, "ProgressDialogFragment") }
                            .doOnTerminate { dialog.dismiss() }
                            .subscribeWith(object : DisposableObserver<DbInfo>() {
                                override fun onNext(dbInfo: DbInfo) {
                                    dbs.add(dbInfo)
                                }

                                override fun onError(e: Throwable) {
                                    Log.e(TAG, "Error: ", e)
                                }

                                override fun onComplete() {
                                    val dbNames = mutableListOf<String>()
                                    dbs.mapTo(dbNames) { it.filename }
                                    val dbListDialog = AppDbsDialogFragment.newInstance(dbNames.toTypedArray(), { index, dbName ->
                                        Log.d(TAG, "db: " + dbName)
                                        val db = dbs[index]
                                        val intent = Intent()
                                        intent.putExtra("filepath", db.absolutePath)
                                        setResult(RESULT_OK, intent)
                                        finish()
                                    })
                                    dbListDialog.show(supportFragmentManager, "AppDbsDialogFragment")
                                }
                            })
            )
        }
        appsRecyclerView.adapter = appsAdapter
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        appsRecyclerView.layoutManager = layoutManager
        val dialog = ProgressDialogFragment.newInstance("Listing apps...")
        mDisposables.add(
                getInstalledApps(true)
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe { dialog.show(supportFragmentManager, "ProgressDialogFragment") }
                        .doOnTerminate { dialog.dismiss() }
                        .subscribeOn(Schedulers.io())
                        .subscribeWith(object : DisposableObserver<AppInfo>() {
                            override fun onNext(appInfo: AppInfo) {
                                apps.add(appInfo)
                                //appsAdapter.notifyItemInserted(apps.size - 1)
                            }

                            override fun onError(e: Throwable) {
                                Log.e(TAG, "Error: ", e)
                            }

                            override fun onComplete() {
                                appsAdapter.notifyDataSetChanged()
                            }
                        })
        )
    }

    private fun checkDb(dbInfo: DbInfo): Boolean {
        // Copy file from owner app folder to our app cache
        val tempFolder = File(filesDir.absolutePath, "temp")
        if (!tempFolder.exists()) {
            tempFolder.mkdirs()
        }
        val tempFile = File(tempFolder, dbInfo.filename)
        if (tempFile.exists()) {
            tempFile.delete()
        }
        tempFile.createNewFile()
        var isValid = RootTools.isAccessGiven() && RootTools.copyFile(dbInfo.absolutePath, tempFile.absolutePath, true, false)
        if (!isValid) return isValid
        var db: SQLiteDatabase? = null
        try {
            db = SQLiteDatabase.openDatabase(tempFile.absolutePath, null, SQLiteDatabase.OPEN_READONLY,
                    DatabaseErrorHandler { Log.e(TAG, "Corrupted database: " + tempFile.absolutePath) })
            db.isDatabaseIntegrityOk
            isValid = true
        }
        catch (e: SQLiteException) {
            Log.e(TAG, "checkDb: Error opening result: " + tempFile.absolutePath + " : " + e.localizedMessage)
            isValid = false
        }
        finally {
            db?.close()
        }
        if (tempFile.exists()) {
            tempFile.delete()
        }
        return isValid
    }

    @SuppressLint("SdCardPath")
    private fun checkDatabaseFolderExists(appInfo: AppsDbChooserActivity.AppInfo): Boolean {
        return RootTools.exists("/data/data/${appInfo.packageName}/databases/", true)
    }

    @SuppressLint("SdCardPath")
    private fun getDatabasesForApp(appInfo: AppInfo): Single<List<DbInfo>> {
        val packageName = appInfo.packageName
        val result = Shell.SU.run("ls /data/data/$packageName/databases/")
        Log.d(TAG, "result: " + result.joinToString(","))
        val dbList = result.map { filename -> DbInfo(filename, "/data/data/$packageName/databases/$filename") }
        return Single.just(dbList)
    }

    private fun getInstalledApps(getSysPackages: Boolean): Observable<AppInfo> {
        val appList = ArrayList<AppInfo>()
        val packages = packageManager.getInstalledPackages(0)
        packages.indices
                .map { packages[it] }
                .filterNot { !getSysPackages && it.versionName == null }
                .mapTo(appList) {
                    AppInfo(it.applicationInfo.loadLabel(packageManager).toString(),
                            it.packageName,
                            it.applicationInfo.loadIcon(packageManager))
                }
        Collections.sort(appList)
        return Observable.fromIterable(appList)
    }

    override fun onDestroy() {
        super.onDestroy()
        mDisposables.clear()
    }

    internal class ProgressDialogFragment : DialogFragment() {
        companion object {
            fun newInstance(message: String): ProgressDialogFragment {
                val args = Bundle()
                args.putString("message", message)
                val dialog = ProgressDialogFragment()
                dialog.arguments = args
                return dialog
            }
        }

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            val message = arguments.getString("message", "null")
            return ProgressDialog(activity).apply {
                isIndeterminate = true
                isCancelable = false
                setMessage(message)
            }
        }
    }

    internal class AppDbsDialogFragment : DialogFragment() {
        private var onItemClick: (index: Int, dbName: String) -> Unit = { _, _ -> }

        companion object {
            fun newInstance(dbNames: Array<String>, listener: (index: Int, dbName: String) -> Unit): AppDbsDialogFragment {
                val args = Bundle()
                args.putStringArray("dbNames", dbNames)
                val fragment = AppDbsDialogFragment()
                fragment.arguments = args
                fragment.onItemClick = listener
                return fragment
            }
        }

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            val dbNames = arguments.getStringArray("dbNames")
            return AlertDialog.Builder(activity).apply {
                setTitle("Select DB")
                if (dbNames.isNotEmpty()) {
                    setItems(dbNames) { _, which ->
                        onItemClick(which, dbNames[which])
                    }
                }
                else {
                    setMessage("No database files found")
                }
                setNeutralButton("Create new") { _, _ -> }
                setNegativeButton("Cancel") { dialog, _ -> dialog.dismiss() }
            }.create()
        }
    }

    internal data class AppInfo(val name: String, val packageName: String, val icon: Drawable) : Comparable<AppInfo> {
        override fun compareTo(other: AppInfo): Int = name.toLowerCase().compareTo(other.name.toLowerCase())
    }

    internal data class DbInfo(val filename: String, val absolutePath: String)
}
