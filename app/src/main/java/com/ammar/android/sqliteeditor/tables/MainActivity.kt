package com.ammar.android.sqliteeditor.tables

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.AssetManager
import android.os.Build
import android.os.Bundle
import android.support.design.widget.*
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.ammar.android.sqliteeditor.R
import com.ammar.android.sqliteeditor.getPath
import com.ammar.android.sqliteeditor.table.TableActivity
import java.io.File
import java.util.*

internal class MainActivity : AppCompatActivity(), MainContract.View {

    private val TAG = MainActivity::class.simpleName
    private val REQUEST_READ_WRITE_STORAGE = 10
    private val FILE_SELECT_CODE = 100
    private val APP_DB_SELECT_CODE = 101
    private var mToolbar: Toolbar? = null
    private var mLoadingView: View? = null
    private var mLoadingMessageView: TextView? = null
    private val mLoadingStack = Stack<kotlin.Any>()
    private var mTableNamesRecyclerView: RecyclerView? = null
    private val mPresenter = MainPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mToolbar = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbar)
        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener { _ -> showAddTableDialog() }
        mLoadingView = findViewById(R.id.loading_view)
        mLoadingMessageView = mLoadingView?.findViewById(R.id.message)
        mTableNamesRecyclerView = findViewById(R.id.table_names)
        mTableNamesRecyclerView?.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        mTableNamesRecyclerView?.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        mPresenter.init()
    }

    override fun checkPermissions(): Boolean {
        return mayReadWriteStorage()
    }

    override fun getAssetsFromView(): AssetManager {
        return assets
    }

    override fun getAppFilesDir(): File {
        return filesDir
    }

    override fun showLoadingMessage(showDialog: Boolean, message: String) {
        if (showDialog) {
            mLoadingStack.push(1)
            mLoadingMessageView?.tag = message
            mLoadingView?.visibility = View.VISIBLE
            return
        }
        if (!mLoadingStack.empty()) mLoadingStack.pop()
        if (mLoadingStack.empty()) mLoadingView?.visibility = View.GONE
    }

    override fun setTableNamesRecyclerViewData(tableNames: List<String>, db: MainContract.Model.Db) {
        val adapter = TableNameAdapter(tableNames)
        adapter.onItemClick { _, tableName, _ ->
            val intent = Intent(this@MainActivity, TableActivity::class.java).apply {
                putExtra(TableActivity.EXTRA_DB_FILE_PATH, db.dbFile.absolutePath)
                putExtra(TableActivity.EXTRA_TABLE_NAME, tableName)
                putExtra(TableActivity.EXTRA_ORIGINAL_FILE_PATH, db.originalPath)
                putExtra(TableActivity.EXTRA_IS_ORIGINAL_PATH_SYSTEM, db.isOriginalPathSystem)
            }
            startActivity(intent)
        }
        mTableNamesRecyclerView?.adapter = adapter
    }

    override fun showErrorDialog(error: String) {
        val errorDisplayDialog = FileOpenErrorAlertDialogFragment.newInstance(error)
        errorDisplayDialog.show(supportFragmentManager, "FileOpenErrorAlertDialogFragment")
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            R.id.open -> {
                val chooseDbFileBottomDialogFragment = ChooseDbFileBottomDialogFragment()
                chooseDbFileBottomDialogFragment.onItemSelected { itemId ->
                    when (itemId) {
                        R.id.choose_apps -> {
                            val intent = Intent(this@MainActivity, AppsDbChooserActivity::class.java)
                            startActivityForResult(intent, APP_DB_SELECT_CODE)
                        }
                        R.id.browse_file -> {
                            val intent = Intent(Intent.ACTION_GET_CONTENT).apply { type = "*/*" }
                            try {
                                startActivityForResult(Intent.createChooser(intent, "Select a File to open"), FILE_SELECT_CODE)
                            }
                            catch (e: ActivityNotFoundException) {
                                // Potentially direct the user to the Market with a Dialog
                                Toast.makeText(this, "Please install a File Manager.", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                }
                chooseDbFileBottomDialogFragment.show(supportFragmentManager, "ChooseDbFileBottomDialogFragment")
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showAddTableDialog() {
        val newTableDialog = NewTableAlertDialogFragment()
        newTableDialog.show(supportFragmentManager, "NewTableAlertDialogFragment")
    }

    private fun mayReadWriteStorage(): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true
        }
        if (checkSelfPermission(READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true
        }
        if (shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE)
                || shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
            Snackbar.make(mToolbar!!, R.string.read_write_storage_permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(arrayOf(READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE), REQUEST_READ_WRITE_STORAGE)
                        }
                    }
        }
        else {
            requestPermissions(arrayOf(READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE), REQUEST_READ_WRITE_STORAGE)
        }
        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            FILE_SELECT_CODE -> {
                if (resultCode == RESULT_OK) {
                    val uri = data?.data
                    //Log.d(TAG, "File Uri: " + uri.toString())
                    val path = uri?.getPath(this@MainActivity)
                    if (path == null) {
                        Log.e(TAG, "path is null")
                        throw IllegalArgumentException("path is null")
                    }
                    mPresenter.openDb(path)
                }
            }
            APP_DB_SELECT_CODE -> {
                if (resultCode == RESULT_OK) {
                    val path = data?.getStringExtra("filepath")
                    if (path == null) {
                        Log.e(TAG, "path is null")
                        throw IllegalArgumentException("path is null")
                    }
                    mPresenter.openDb(path)
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == REQUEST_READ_WRITE_STORAGE) {
            if (grantResults.size == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                mPresenter.init()
            }
        }
    }

    override fun onDestroy() {
        mPresenter.clearDisposables()
        super.onDestroy()
    }

    internal class ChooseDbFileBottomDialogFragment : BottomSheetDialogFragment(), View.OnClickListener {

        private var mOnItemSelected: (Int) -> Unit = {}

        private val mBottomSheetBehaviorCallback = object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    dismiss()
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        }

        override fun setupDialog(dialog: Dialog, style: Int) {
            val contentView = View.inflate(context, R.layout.content_choose_db_file_bottom_sheet, null)
            dialog.setContentView(contentView)
            val params = (contentView.parent as View).layoutParams as CoordinatorLayout.LayoutParams
            val behavior = params.behavior as CoordinatorLayout.Behavior
            if (behavior is BottomSheetBehavior) {
                behavior.setBottomSheetCallback(mBottomSheetBehaviorCallback)
            }
            contentView.findViewById<View>(R.id.choose_apps).setOnClickListener(this)
            contentView.findViewById<View>(R.id.browse_file).setOnClickListener(this)
        }

        override fun onClick(v: View) {
            mOnItemSelected(v.id)
            dismiss()
        }

        fun onItemSelected(listener: (Int) -> Unit) {
            mOnItemSelected = listener
        }
    }

    internal class FileOpenErrorAlertDialogFragment : DialogFragment() {

        companion object {
            fun newInstance(error: String): FileOpenErrorAlertDialogFragment {
                val fragment = FileOpenErrorAlertDialogFragment()
                val args = Bundle()
                args.putString("error", error)
                fragment.arguments = args
                return fragment
            }
        }

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

            val error = arguments?.getString("error")
            return AlertDialog.Builder(activity).apply {
                setMessage(error)
                setPositiveButton("OK", { dialog, _ -> dialog.dismiss() })
            }.create()
        }
    }
}
