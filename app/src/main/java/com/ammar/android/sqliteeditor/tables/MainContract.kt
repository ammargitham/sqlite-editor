package com.ammar.android.sqliteeditor.tables

import android.content.res.AssetManager
import io.reactivex.Single
import java.io.File

/**
 * Created by agitham on 31/8/2017.
 */
internal interface MainContract {

    interface View {
        fun checkPermissions(): Boolean
        fun getAssetsFromView(): AssetManager
        fun getAppFilesDir(): File
        fun showLoadingMessage(showDialog: Boolean, message: String)
        fun setTableNamesRecyclerViewData(tableNames: List<String>, db: MainContract.Model.Db)
        fun showErrorDialog(error: String)
    }

    interface Presenter {

        fun init()
        fun openDb(db: Model.Db)
        fun openDb(filePath: String)
        fun clearDisposables()
    }

    interface Model {

        fun getTestDbFile(assets: AssetManager): Db
        fun openDb(dbFile: File): List<String>
        fun openDb(dbFilePath: String, filesDir: File): Single<Db>
        fun checkFile(db: Db): Pair<Db, FileCheckResult>

        data class Db(val dbFile: File, val originalPath: String, val isOriginalPathSystem: Boolean)
        data class FileCheckResult(val isValid: Boolean, val error: String)
    }
}