package com.ammar.android.sqliteeditor.tables

import android.content.res.AssetManager
import android.database.DatabaseErrorHandler
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.os.Environment
import android.support.v4.os.EnvironmentCompat
import android.util.Log
import com.ammar.android.sqliteeditor.utils.SqliteHelper
import com.stericson.RootTools.RootTools
import io.reactivex.Single
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.nio.channels.FileChannel
import java.util.*
import java.util.concurrent.Callable

/**
 * Created by agitham on 1/9/2017.
 */
internal class MainModel : MainContract.Model {

    private val TAG = MainModel::class.simpleName

    override fun getTestDbFile(assets: AssetManager): MainContract.Model.Db {
        val dir = Environment.getExternalStorageDirectory()
        if (dir == null) {
            Log.e(TAG, "init: getExternalStorageDirectory is null")
            throw FileNotFoundException("getExternalStorageDirectory not found")
        }
        val dbFile = File(dir, "test.db")
        val dbFileExists = dbFile.exists()
        if (!dbFileExists) {
            dbFile.createNewFile()
            // Copy db from assests to external storage
            copyTestDbFromAssets(dbFile, assets)
        }
        return MainContract.Model.Db(dbFile, dbFile.absolutePath, false)
    }

    override fun openDb(dbFile: File): List<String> {
        var db: SQLiteDatabase? = null
        try {
            db = SQLiteDatabase.openDatabase(dbFile.absolutePath, null, SQLiteDatabase.OPEN_READONLY,
                    DatabaseErrorHandler { Log.e(TAG, "Corrupted database file: " + dbFile.absolutePath) })
            return SqliteHelper.getTableNames(db)
        }
        catch (e: SQLiteException) {
            Log.e(TAG, "openDb: ", e)
        }
        finally {
            db?.close()
        }
        return Collections.emptyList<String>()
    }

    override fun openDb(dbFilePath: String, filesDir: File): Single<MainContract.Model.Db> {
        return Single.fromCallable(
                Callable<MainContract.Model.Db> {
                    Log.d(TAG, "File Path: " + dbFilePath)
                    var selectedFile = File(dbFilePath)
                    val storageState = EnvironmentCompat.getStorageState(selectedFile)
                    var isSystemPath = false
                    if (storageState == EnvironmentCompat.MEDIA_UNKNOWN && RootTools.isAccessGiven()) {
                        isSystemPath = true
                        // Copying file to app specific folder for easier IO operations
                        val tempFile = File(filesDir, selectedFile.name)
                        if (tempFile.exists()) {
                            tempFile.delete()
                        }
                        tempFile.createNewFile()
                        RootTools.copyFile(dbFilePath, tempFile.absolutePath, true, false)
                        selectedFile = tempFile
                    }
                    return@Callable MainContract.Model.Db(selectedFile, dbFilePath, isSystemPath)
                })
    }

    private fun copyTestDbFromAssets(targetFile: File, assets: AssetManager) {
        Log.d(TAG, "Copying test.db from assets")
        var inputChannel: FileChannel? = null
        var outputChannel: FileChannel? = null
        try {
            val inputAfd = assets.openFd("test.db")
            inputChannel = inputAfd.createInputStream().channel
            outputChannel = FileOutputStream(targetFile).channel
            inputChannel.transferTo(inputAfd.startOffset, inputAfd.length, outputChannel)
        }
        catch (e: IOException) {
            Log.e(TAG, "Error in copying assets file", e)
        }
        finally {
            inputChannel?.close()
            outputChannel?.close()
        }
    }

    override fun checkFile(db: MainContract.Model.Db): Pair<MainContract.Model.Db, MainContract.Model.FileCheckResult> {
        var isValid: Boolean
        var error = ""
        var sqLiteDatabase: SQLiteDatabase? = null
        try {
            sqLiteDatabase = SQLiteDatabase.openDatabase(db.dbFile.absolutePath, null, SQLiteDatabase.OPEN_READONLY,
                    DatabaseErrorHandler { Log.e(TAG, "Corrupted database result: " + db.dbFile.absolutePath) })
            sqLiteDatabase.isDatabaseIntegrityOk
            isValid = true
        }
        catch (e: SQLiteException) {
            Log.e(TAG, "checkFileAndOpen: Error opening result: " + db.dbFile.absolutePath, e)
            isValid = false
            error = e.localizedMessage
        }
        finally {
            sqLiteDatabase?.close()
        }
        return Pair(db, MainContract.Model.FileCheckResult(isValid, error))
    }
}