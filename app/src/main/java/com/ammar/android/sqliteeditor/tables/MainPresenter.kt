package com.ammar.android.sqliteeditor.tables

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.io.File

/**
 * Created by agitham on 1/9/2017.
 */
internal class MainPresenter(view: MainContract.View) : MainContract.Presenter {

    //private val TAG = MainPresenter::class.simpleName
    private val mView: MainContract.View = view
    private val mModel: MainContract.Model = MainModel()
    private var mDisposables = CompositeDisposable()

    override fun init() {
        if (!mView.checkPermissions()) {
            return
        }
        mView.showLoadingMessage(true, "Copying test db from assets...")
        val db = mModel.getTestDbFile(mView.getAssetsFromView())
        openDb(db)
        mView.showLoadingMessage(false, "")
    }

    override fun openDb(db: MainContract.Model.Db) {
        mView.showLoadingMessage(true, "Reading " + db.dbFile.name + "...")
        val tableNames = mModel.openDb(db.dbFile)
        mView.setTableNamesRecyclerViewData(tableNames, db)
        mView.showLoadingMessage(false, "")
    }

    override fun openDb(filePath: String) {
        val dbFile = File(filePath)
        val dbSingle = mModel.openDb(filePath, mView.getAppFilesDir())
        val disposable = dbSingle
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { mView.showLoadingMessage(true, "Reading " + dbFile.name + "...") }
                .observeOn(Schedulers.io())
                .map { db -> mModel.checkFile(db) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess { mView.showLoadingMessage(false, "") }
                .subscribe { (db, fileCheckResult) ->
                    if (fileCheckResult.isValid) {
                        openDb(db)
                        return@subscribe
                    }
                    mView.showErrorDialog(fileCheckResult.error)
                    //val errorDisplayDialog = MainActivity.FileOpenErrorAlertDialogFragment.newInstance(fileCheckResult.error)
                    //errorDisplayDialog.show(supportFragmentManager, "FileOpenErrorAlertDialogFragment")
                }
        mDisposables.add(disposable)
    }

    override fun clearDisposables() {
        mDisposables.clear()
    }
}