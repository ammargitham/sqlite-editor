package com.ammar.android.sqliteeditor.tables

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import com.ammar.android.sqliteeditor.R
import com.ammar.android.sqliteeditor.getTextWatcherObservable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Function

/**
 * Created by agitham on 12/6/2017.
 */
internal class NewTableAlertDialogFragment : DialogFragment() {

    private val TAG = "NewTableAlertDialogFragment"
    private var positiveButton: Button? = null
    private var disposable: Disposable? = null
    //private var buttonEnabled: Boolean = false

    fun Observable<String>.getColumnNameValidationObservable(editText: EditText): Observable<Boolean> {
        return this.observeOn(AndroidSchedulers.mainThread())
                .map(Function<String, Boolean> { input ->
                    if (input.isEmpty()) {
                        editText.error = "Enter a name for the column"
                        return@Function false
                    }
                    true
                })
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val sources = ArrayList<Observable<Boolean>>()
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_add_table, null, false)
        val formView = view.findViewById<ViewGroup>(R.id.form_container)
        val addColumnView = view.findViewById<View>(R.id.add_column)
        val name1EditText = view.findViewById<EditText>(R.id.name_1)
        var count = 1
        name1EditText.id = count
        val fieldObservable1 = name1EditText.getTextWatcherObservable().getColumnNameValidationObservable(name1EditText)
        sources.add(fieldObservable1)
        combineSources(sources)
        addColumnView.setOnClickListener {
            val childView = LayoutInflater.from(context).inflate(R.layout.content_add_table_column_input_template, formView, false)
            val nameEditText = childView.findViewById<EditText>(R.id.name_1)
            nameEditText.id = (++count)
            formView.addView(childView)
            val fieldObservable = nameEditText.getTextWatcherObservable().getColumnNameValidationObservable(nameEditText).startWith { Observable.just(false) }
            sources.add(fieldObservable)
            combineSources(sources)
        }
        val dialog = AlertDialog.Builder(activity).apply {
            setTitle("New table")
            setView(view)
            setPositiveButton("Add", { dialog, _ ->
                dialog.dismiss()
            })
            setNegativeButton("Cancel", { dialog, _ -> dialog.dismiss() })
        }.create()
        dialog.setOnShowListener { dialog ->
            positiveButton = (dialog as AlertDialog).getButton(DialogInterface.BUTTON_POSITIVE)
            positiveButton?.isEnabled = false
            /*// Emitting the already input text to the textWatcherObservables to trigger Observable.combineLatest
            (0..fieldSubjects.size() - 1)
                    .map { fieldSubjects.keyAt(it) }
                    .forEach { fieldSubjects.get(it).onNext(fieldValues.get(it)) }*/
        }
        return dialog
    }

    private fun combineSources(sources: ArrayList<Observable<Boolean>>) {
        disposable?.dispose()
        disposable = Observable.combineLatest(sources) { objects ->
            var result = false
            for (`object` in objects) {
                result = `object` as Boolean
                if (!result) {
                    break
                }
            }
            Log.d(TAG, "result: " + result)
            result
        }.subscribe { result -> positiveButton?.isEnabled = result }
    }

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
        disposable?.dispose()
    }
}