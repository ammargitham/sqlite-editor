package com.ammar.android.sqliteeditor.tables

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.ammar.android.sqliteeditor.R

/**
 * Created by Ammar Githam on 11-05-2017.
 */
internal class TableNameAdapter(private val mTableNames: List<String>) : RecyclerView.Adapter<TableNameAdapter.ViewHolder>() {
    private var mOnItemClick: (position: Int, tableName: String, v: View) -> Unit = { _, _, _ -> }

    internal inner class ViewHolder(itemView: TextView) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private var tableName: String? = null

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            mOnItemClick(adapterPosition, tableName ?: "", v)
        }

        fun setTableName(tableName: String) {
            this.tableName = tableName
        }
    }

    fun onItemClick(listener: (position: Int, tableName: String, v: View) -> Unit) {
        mOnItemClick = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TableNameAdapter.ViewHolder {
        val tableNameView = LayoutInflater.from(parent.context).inflate(R.layout.content_table_name_item, parent, false) as TextView
        return ViewHolder(tableNameView)
    }

    override fun onBindViewHolder(holder: TableNameAdapter.ViewHolder, position: Int) {
        val tableName = mTableNames[position]
        (holder.itemView as TextView).text = tableName
        holder.setTableName(tableName)
    }

    override fun getItemCount(): Int {
        return mTableNames.size
    }
}
