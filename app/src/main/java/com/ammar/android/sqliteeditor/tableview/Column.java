package com.ammar.android.sqliteeditor.tableview;

public class Column<T> {

    private       T      headerItem;
    private final String headerText;
    private       int    columnWidth;
    private       int    width;
    private       float  weight;
    private       int    viewWidth;

    public Column(T headerItem, String headerText) {
        this.headerText = headerText;
        this.headerItem = headerItem;
    }

    public T getHeaderItem() {
        return this.headerItem;
    }

    public void setViewWidth(int viewWidth) {
        this.viewWidth = viewWidth;
    }

    public void setWidthAndWeight(int width, float weight) {
        this.columnWidth = 0;
        this.width = width;
        this.weight = weight;
    }

    public String getHeaderText() {
        return this.headerText;
    }

    public void setColumnWidth(int columnWidth) {
        this.columnWidth = columnWidth;
        this.width = 0;
        this.weight = 0.0f;
    }

    public int getColumnWidth() {
        return this.columnWidth;
    }

    public float getWeight() {
        return this.weight;
    }

    public int getWidth() {
        return this.width;
    }

    public boolean equals(Object obj) {
        return (obj instanceof Column) && obj.hashCode() == hashCode();
    }

    public int hashCode() {
        return this.headerText.hashCode();
    }

    @Override
    public String toString() {
        return "Column{" +
                "headerItem=" + headerItem +
                ", headerText='" + headerText + '\'' +
                ", columnWidth=" + columnWidth +
                ", width=" + width +
                ", weight=" + weight +
                ", viewWidth=" + viewWidth +
                '}';
    }
}
