package com.ammar.android.sqliteeditor.tableview;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Grid<T> {

    private T[][]           grid;
    private List<Column<T>> columnList;
    private List<Row<T>>    rowList;

    @SuppressWarnings("unchecked")
    protected void setGridHeaders(List<Column<T>> columns, List<Row<T>> rows, int height) {
        this.columnList = new ArrayList<>(columns);
        this.rowList = new ArrayList<>(rows);
        Row<T> row = new Row<>(null, ((Column) columns.get(0)).getHeaderText());
        row.setHeight(height);
        this.rowList.add(0, row);
        this.grid = (T[][]) Array.newInstance(Object.class, this.rowList.size(), this.columnList.size());
        for (int i = 0; i < this.columnList.size(); i++) {
            setItem(i, 0, this.columnList.get(i).getHeaderItem());
        }
        for (int i = 1; i < this.rowList.size(); i++) {
            setItem(0, i, this.rowList.get(i).getHeaderItem());
        }
    }

    public int getColumnNo(int position) {
        return position / getColumnCount();
    }

    public int getTotalSize() {
        return getColumnCount() * getRowCount();
    }

    public int getRowNo(int position) {
        return getColumnCount() - (((getColumnNo(position) + 1) * getColumnCount()) - position);
    }

    public T getItem(int column, int row) {
        return this.grid[row][column];
    }

    public void setItem(int column, int row, T item) {
        if (column != -1 && row != -1) {
            this.grid[row][column] = item;
        }
    }

    public int getColumnCount() {
        return this.columnList.size();
    }

    public Column<T> getColumnHeader(int columnNo) {
        return this.columnList.get(columnNo);
    }

    public int getRowCount() {
        return this.rowList.size();
    }

    public Row<T> getRow(int rowNo) {
        return rowList.get(rowNo);
    }
}
