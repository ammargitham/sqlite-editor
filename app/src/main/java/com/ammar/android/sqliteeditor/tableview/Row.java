package com.ammar.android.sqliteeditor.tableview;

public class Row<T> {

    private       T      headerItem;
    private final String headerText;
    private       int    height;

    public Row(T t, String headerText) {
        this.headerItem = t;
        this.headerText = headerText;
    }

    public T getHeaderItem() {
        return this.headerItem;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getHeight() {
        return this.height;
    }

    public String getHeaderText() {
        return this.headerText;
    }

    public boolean equals(Object obj) {
        return (obj instanceof Row) && obj.hashCode() == hashCode();
    }

    public int hashCode() {
        return this.headerText.hashCode();
    }

    @Override
    public String toString() {
        return "Row{" +
                "headerItem=" + headerItem +
                ", height=" + height +
                ", headerText='" + headerText + '\'' +
                '}';
    }
}
