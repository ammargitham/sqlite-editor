package com.ammar.android.sqliteeditor.tableview;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.ammar.android.sqliteeditor.R;

public abstract class TableAdapter<T> extends Adapter<RecyclerView.ViewHolder> implements OnClickListener {

    private Grid<T>                  grid;
    private OnTableCellClickListener onTableCellClickListener;

    public interface OnTableCellClickListener {

        void onCellClick(int x, int y, View view, Object obj);
    }

    public TableAdapter(Grid<T> grid, OnTableCellClickListener onItemClickListener) {
        this.grid = grid;
        this.onTableCellClickListener = onItemClickListener;
    }

    @Override
    public void onClick(View view) {
        if (this.onTableCellClickListener != null) {
            int position = (Integer) view.getTag(R.id.table_cell_tag_data_position);
            int x = grid.getRowNo(position);
            int y = grid.getColumnNo(position);
            this.onTableCellClickListener.onCellClick(x, y, view, grid.getItem(x, y));
        }
    }

    @Override
    public int getItemCount() {
        return this.grid.getTotalSize();
    }

    @Override
    public int getItemViewType(int position) {
        int row = this.grid.getColumnNo(position);
        int column = this.grid.getRowNo(position);
        return this.getItemViewType(row, column, this.grid.getItem(column, row));
    }

    public abstract int getItemViewType(int row, int column, T item);

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        int row = this.grid.getColumnNo(position);
        int column = this.grid.getRowNo(position);
        T item = this.grid.getItem(column, row);
        if (this.isClickable(row, column, item)) {
            viewHolder.itemView.setOnClickListener(this);
        }
        else {
            viewHolder.itemView.setOnClickListener(null);
        }
        this.onBindViewHolder(row, column, viewHolder.itemView, item);
    }

    @Override
    abstract public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType);

    @Override
    public abstract void onViewRecycled(RecyclerView.ViewHolder viewHolder);

    public abstract void onBindViewHolder(int row, int column, View itemView, T item);

    public abstract boolean isClickable(int row, int column, T item);

    public Grid<T> getGrid() {
        return grid;
    }

    public void setGrid(Grid<T> grid) {
        this.grid = grid;
    }

    public OnTableCellClickListener getOnTableCellClickListener() {
        return this.onTableCellClickListener;
    }

    public void setOnTableCellClickListener(OnTableCellClickListener onTableCellClickListener) {
        this.onTableCellClickListener = onTableCellClickListener;
    }
}
