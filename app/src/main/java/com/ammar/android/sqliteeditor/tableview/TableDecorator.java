package com.ammar.android.sqliteeditor.tableview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.ammar.android.sqliteeditor.R;

public class TableDecorator implements TableRecyclerView.ItemDecorator {

    private boolean  isBottomShadowEnabled;
    private boolean  isRightShadowEnabled;
    private boolean  isHorizontalDividersEnabled;
    private boolean  isVerticalDividersEnabled;
    private boolean  enableHorizontalDividerForHeader;
    private Drawable bottomShadowDrawable;
    private Drawable rightShadowDrawable;
    private int      horizontalDividerHeight;
    private Paint    horizontalDividerPaint;
    private Paint    verticalDividerPaint;
    private int      verticalDividerWidth;
    private int      shadowSpread;
    private int      shadowSize;
    private Rect     shadowBounds;
    private Drawable f6532o;

    public static class Builder {

        private final TableDecorator tableDecorator;

        public Builder() {
            this.tableDecorator = new TableDecorator();
        }

        public Builder setSomeColor(int i) {
            return m11319a(new ColorDrawable(i));
        }

        public Builder setHorizontalDivider(int color, int height) {
            return setHorizontalDivider(color, height, true);
        }

        public Builder setHorizontalDivider(int color, int height, boolean enableDividerForHeader) {
            this.tableDecorator.isHorizontalDividersEnabled = true;
            this.tableDecorator.horizontalDividerPaint = new Paint();
            this.tableDecorator.horizontalDividerPaint.setColor(color);
            this.tableDecorator.horizontalDividerHeight = height;
            this.tableDecorator.enableHorizontalDividerForHeader = enableDividerForHeader;
            return this;
        }

        public Builder enableBottomShadow(Context context) {
            Drawable drawable = ContextCompat.getDrawable(context, R.drawable.table_shadow_bottom);
            int size = context.getResources().getDimensionPixelSize(R.dimen.table_shadow_size);
            this.tableDecorator.shadowSpread = size * 3;
            return setBottomShadow(drawable, size);
        }

        public Builder m11319a(Drawable drawable) {
            this.tableDecorator.f6532o = drawable;
            return this;
        }

        public Builder setBottomShadow(Drawable drawable, int i) {
            this.tableDecorator.isBottomShadowEnabled = true;
            this.tableDecorator.bottomShadowDrawable = drawable;
            this.tableDecorator.shadowSize = i;
            return this;
        }

        public TableDecorator build() {
            return this.tableDecorator;
        }

        public Builder setVerticalDivider(int color, int width) {
            this.tableDecorator.isVerticalDividersEnabled = true;
            this.tableDecorator.verticalDividerPaint = new Paint();
            this.tableDecorator.verticalDividerPaint.setColor(color);
            this.tableDecorator.verticalDividerWidth = width;
            return this;
        }

        public Builder enableRightShadow(Context context) {
            Drawable drawable = ContextCompat.getDrawable(context, R.drawable.table_shadow_right);
            int size = context.getResources().getDimensionPixelSize(R.dimen.table_shadow_size);
            this.tableDecorator.shadowSpread = size * 3;
            return setRightShadow(drawable, size);
        }

        public Builder setRightShadow(Drawable drawable, int size) {
            this.tableDecorator.isRightShadowEnabled = true;
            this.tableDecorator.rightShadowDrawable = drawable;
            this.tableDecorator.shadowSize = size;
            return this;
        }
    }

    private TableDecorator() {
        this.shadowBounds = new Rect();
    }

    private float getShadowSpread(int i) {
        return i >= this.shadowSpread ? 1.0f : (1.0f * ((float) i)) / ((float) this.shadowSpread);
    }

    private void drawHorizontalDividers(TableRecyclerView tableRecyclerView, Canvas canvas) {
        int width = Math.min(tableRecyclerView.getWidth(), tableRecyclerView.getWidths()[tableRecyclerView.getWidths().length - 1]);
        if (tableRecyclerView.isColumnHeadersEnabled() && this.enableHorizontalDividerForHeader) {
            int height = tableRecyclerView.getHeights()[1];
            canvas.drawRect(0.0f, (float) (height - this.horizontalDividerHeight), (float) width, (float) height, this.horizontalDividerPaint);
        }
        int[] heights = tableRecyclerView.getHeights();
        int scrollableFirstVisibleRow = this.enableHorizontalDividerForHeader ? tableRecyclerView.getScrollableFirstVisibleRow() : Math.max(1,
                tableRecyclerView.getScrollableFirstVisibleRow());
        while (scrollableFirstVisibleRow <= tableRecyclerView.getScrollableLastVisibleRow()) {
            int height = heights[scrollableFirstVisibleRow + 1] - tableRecyclerView.getVerticalScrollOffset();
            if (height - this.horizontalDividerHeight <= tableRecyclerView.getHeight()) {
                canvas.drawRect(0.0f, (float) (height - this.horizontalDividerHeight), (float) width, (float) height, this.horizontalDividerPaint);
                scrollableFirstVisibleRow++;
            }
            else {
                return;
            }
        }
    }

    private void drawVerticalDividers(TableRecyclerView tableRecyclerView, Canvas canvas) {
        int min = Math.min(tableRecyclerView.getHeight(), tableRecyclerView.getHeights()[tableRecyclerView.getHeights().length - 1]);
        if (tableRecyclerView.isRowHeadersEnabled()) {
            int i = tableRecyclerView.getWidths()[1];
            canvas.drawRect((float) (i - this.verticalDividerWidth), 0.0f, (float) i, (float) min, this.verticalDividerPaint);
        }
        int[] widths = tableRecyclerView.getWidths();
        int scrollableFirstVisibleColumn = tableRecyclerView.getScrollableFirstVisibleColumn();
        while (scrollableFirstVisibleColumn <= tableRecyclerView.getScrollableLastVisibleColumn()) {
            int i = widths[scrollableFirstVisibleColumn + 1] - tableRecyclerView.getHorizontalScrollOffset();
            if (i - this.verticalDividerWidth <= tableRecyclerView.getWidth()) {
                canvas.drawRect((float) (i - this.verticalDividerWidth), 0.0f, (float) i, (float) min, this.verticalDividerPaint);
                scrollableFirstVisibleColumn++;
            }
            else {
                return;
            }
        }
    }

    private void drawRightShadow(TableRecyclerView tableRecyclerView, Canvas canvas) {
        int i = tableRecyclerView.getWidths()[1];
        this.shadowBounds.set(i, 0, ((int) (((float) this.shadowSize) * getShadowSpread(tableRecyclerView.getHorizontalScrollOffset()))) + i,
                Math.min(tableRecyclerView.getHeight(), tableRecyclerView.getHeights()[tableRecyclerView.getHeights().length - 1]));
        if (!this.shadowBounds.equals(this.rightShadowDrawable.getBounds())) {
            this.rightShadowDrawable.setBounds(this.shadowBounds);
        }
        this.rightShadowDrawable.draw(canvas);
    }

    private void drawBottomShadow(TableRecyclerView tableRecyclerView, Canvas canvas) {
        int headerHeight = tableRecyclerView.getHeights()[1];
        this.shadowBounds.set(0, headerHeight,
                Math.min(tableRecyclerView.getWidth(), tableRecyclerView.getWidths()[tableRecyclerView.getWidths().length - 1]),
                ((int) (((float) this.shadowSize) * getShadowSpread(tableRecyclerView.getVerticalScrollOffset()))) + headerHeight);
        if (!this.shadowBounds.equals(this.bottomShadowDrawable.getBounds())) {
            this.bottomShadowDrawable.setBounds(this.shadowBounds);
        }
        this.bottomShadowDrawable.draw(canvas);
    }

    private void m11345e(TableRecyclerView recyclerView, Canvas canvas) {
        int min = Math.min(recyclerView.getWidth(), recyclerView.getWidths()[recyclerView.getWidths().length - 1]);
        int offset = recyclerView.isColumnHeadersEnabled() ? 0 : -recyclerView.getVerticalScrollOffset();
        this.shadowBounds.set(0, 0, min, recyclerView.getHeights()[1]);
        if (!this.shadowBounds.equals(this.f6532o.getBounds())) {
            this.f6532o.setBounds(this.shadowBounds);
        }
        if (offset != 0) {
            canvas.save();
            canvas.translate(0.0f, (float) offset);
        }
        this.f6532o.draw(canvas);
        if (offset != 0) {
            canvas.restore();
        }
    }

    @Override
    public void drawDividersAndShadows(Canvas canvas, TableRecyclerView recyclerView) {
        if (isHorizontalDividersEnabled) {
            drawHorizontalDividers(recyclerView, canvas);
        }
        if (isVerticalDividersEnabled) {
            drawVerticalDividers(recyclerView, canvas);
        }
        if (isBottomShadowEnabled && recyclerView.getVerticalScrollOffset() > 0 && recyclerView.isColumnHeadersEnabled()) {
            drawBottomShadow(recyclerView, canvas);
        }
        if (isRightShadowEnabled && recyclerView.getHorizontalScrollOffset() > 0 && recyclerView.isRowHeadersEnabled()) {
            drawRightShadow(recyclerView, canvas);
        }
    }

    @Override
    public void m11308b(Canvas canvas, TableRecyclerView recyclerView) {
        if (this.f6532o == null) {
            return;
        }
        if (recyclerView.isColumnHeadersEnabled() || recyclerView.getScrollableFirstVisibleRow() == 0) {
            m11345e(recyclerView, canvas);
        }
    }
}
