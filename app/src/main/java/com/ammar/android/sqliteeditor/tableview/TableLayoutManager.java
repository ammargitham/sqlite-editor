package com.ammar.android.sqliteeditor.tableview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.support.v7.widget.RecyclerView.Recycler;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ammar.android.sqliteeditor.R;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class TableLayoutManager extends LayoutManager {

    private static final String TAG = "TableLayoutManager";
    boolean isInitialPass;
    int[]   rowVerticalOffsets;
    int[]   columnHorizontalOffsets;
    int     scrollableFirstVisibleColumn;
    int     scrollableFirstVisibleRow;
    int     scrollableLastVisibleColumn;
    int     scrollableLastVisibleRow;
    int     horizontalScrollOffset;
    int     verticalScrollOffset;
    private final Rect               normalCellRect;
    private final Rect               rowHeaderRect;
    private final Rect               columnHeaderRect;
    private final Context            context;
    private final Map<Integer, View> children;
    private       boolean            isRowHeadersEnabled;
    private       boolean            isColumnHeadersEnabled;
    private       boolean            isLeftToRight;
    private       int                firstVisiblePosition;
    private       int                someHorizontalOffset;
    private       TableAdapter       adapter;

    TableLayoutManager(Context context) {
        isInitialPass = true;
        normalCellRect = new Rect();
        rowHeaderRect = new Rect();
        columnHeaderRect = new Rect();
        isRowHeadersEnabled = true;
        isColumnHeadersEnabled = true;
        isLeftToRight = true;
        scrollableFirstVisibleColumn = -1;
        scrollableFirstVisibleRow = -1;
        scrollableLastVisibleColumn = -1;
        scrollableLastVisibleRow = -1;
        children = new HashMap<>();
        this.context = context;
    }

    @Override
    public LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onLayoutChildren(Recycler recycler, State state) {
        if (children.isEmpty() || isInitialPass) {
            isInitialPass = false;
            removeAndRecycleAllViews(recycler);
            children.clear();
            columnHorizontalOffsets = null;
            rowVerticalOffsets = null;
            scrollableFirstVisibleColumn = -1;
            scrollableLastVisibleColumn = -1;
            scrollableFirstVisibleRow = -1;
            scrollableLastVisibleRow = -1;
            if (getGrid() != null) {
                setUpBounds();
                addAllViews(recycler);
                layoutAllViews();
            }
        }
    }

    private void setUpBounds() {
        if (getGrid() == null) {
            return;
        }
        if (columnHorizontalOffsets == null) {
            columnHorizontalOffsets = new int[(getGrid().getColumnCount() + 1)];
            rowVerticalOffsets = new int[(getGrid().getRowCount() + 1)];
            setColumnAndRowDimens(context, getGrid(), columnHorizontalOffsets, rowVerticalOffsets, getWidth(), getHeight());
            verticalScrollOffset = Math.min(Math.max(0, rowVerticalOffsets[rowVerticalOffsets.length - 1] - getHeight()), verticalScrollOffset);
            horizontalScrollOffset = Math.min(columnHorizontalOffsets[columnHorizontalOffsets.length - 1] - getWidth(), horizontalScrollOffset);
            if (firstVisiblePosition != 0) {
                updateHorizontalScrollOffset(firstVisiblePosition);
                firstVisiblePosition = 0;
            }
            if (someHorizontalOffset != 0) {
                updateHorizontalOffset(someHorizontalOffset);
                someHorizontalOffset = 0;
            }
        }
        int left;
        int right;
        if (!isRowHeadersEnabled()) {
            right = 0;
            left = 0;
        }
        else if (isLeftToRight()) {
            left = getFirstWidth();
            right = 0;
        }
        else {
            right = getFirstWidth();
            left = 0;
        }
        int top = isColumnHeadersEnabled() ? getColumnHeaderHeight() : 0;
        normalCellRect.set(left, top, getWidth() - right, getHeight());
        if (isLeftToRight()) {
            rowHeaderRect.set(0, top, getFirstWidth(), getHeight());
        }
        else {
            rowHeaderRect.set(getWidth() - getFirstWidth(), top, getWidth(), getHeight());
        }
        columnHeaderRect.set(left, 0, getWidth() - right, top);
        columnHeaderRect.offset(horizontalScrollOffset, 0);
        rowHeaderRect.offset(0, verticalScrollOffset);
        normalCellRect.offset(horizontalScrollOffset, verticalScrollOffset);
    }

    private void addAllViews(Recycler recycler) {
        if (getGrid() == null) {
            return;
        }
        int firstVisibleRow = indexOfKey(rowVerticalOffsets, normalCellRect.top + 1);
        int lastVisibleRow = indexOfKey(rowVerticalOffsets, Math.min(rowVerticalOffsets[rowVerticalOffsets.length - 1], normalCellRect.bottom));
        int firstVisibleColumn = indexOfKey(columnHorizontalOffsets, isLeftToRight() ?
                normalCellRect.left + 1 : ((columnHorizontalOffsets[columnHorizontalOffsets.length - 1] - horizontalScrollOffset) - normalCellRect.width()) + 1);
        int lastVisibleColumn = indexOfKey(columnHorizontalOffsets, isLeftToRight() ? Math.min(columnHorizontalOffsets[columnHorizontalOffsets.length - 1], normalCellRect.right) :
                columnHorizontalOffsets[columnHorizontalOffsets.length - 1] - horizontalScrollOffset);
        if (firstVisibleRow != scrollableFirstVisibleRow || lastVisibleRow != scrollableLastVisibleRow ||
                firstVisibleColumn != scrollableFirstVisibleColumn ||
                lastVisibleColumn != scrollableLastVisibleColumn) {
            if (scrollableFirstVisibleRow == -1) {
                children.clear();
                for (int i = firstVisibleRow; i <= lastVisibleRow; i++) {
                    for (int j = firstVisibleColumn; j <= lastVisibleColumn; j++) {
                        int position = (getGrid().getColumnCount() * i) + j;
                        children.put(position, getView(position, recycler));
                    }
                    if (isRowHeadersEnabled()) {
                        int position = getGrid().getColumnCount() * i;
                        children.put(position, getView(position, recycler));
                    }
                }
                if (isColumnHeadersEnabled()) {
                    for (int i = firstVisibleColumn; i <= lastVisibleColumn; i++) {
                        children.put(i, getView(i, recycler));
                    }
                }
                if (isRowHeadersEnabled() && isColumnHeadersEnabled() && !children.containsKey(0)) {
                    children.put(0, getView(0, recycler));
                }
                for (Entry entry : children.entrySet()) {
                    addView((Integer) entry.getKey(), (View) entry.getValue());
                }
            }
            else {
                recycleFirstRow(recycler, firstVisibleRow);
                recycleLastRow(recycler, lastVisibleRow);
                recycleFirstColumn(recycler, firstVisibleColumn);
                recycleLastColumn(recycler, lastVisibleColumn);
            }
            scrollableFirstVisibleRow = firstVisibleRow;
            scrollableLastVisibleRow = lastVisibleRow;
            scrollableFirstVisibleColumn = firstVisibleColumn;
            scrollableLastVisibleColumn = lastVisibleColumn;
        }
    }

    private void addView(int position, View view) {
        if (position == 0) {
            addView(view);
            return;
        }
        int childCount = getChildCount() - 1;
        if (isRowHeader(position) || isColumnHeader(position)) {
            addView(view, childCount - 1);
        }
        else {
            addView(view, 0);
        }
    }

    private void layoutAllViews() {
        for (Entry entry : children.entrySet()) {
            int position = (Integer) entry.getKey();
            View child = (View) entry.getValue();
            layoutView(child, position, (Rect) child.getTag(R.id.table_cell_tag_bounds));
        }
    }

    private void layoutView(View view, int position, Rect bounds) {
        if (isColumnHeader(position) && isRowHeader(position)) {
            layoutDecorated(view, bounds.left, bounds.top, bounds.right, bounds.bottom);
        }
        else if (isColumnHeader(position)) {
            layoutDecorated(view, bounds.left - horizontalScrollOffset, bounds.top, bounds.right - horizontalScrollOffset, bounds.bottom);
        }
        else if (isRowHeader(position)) {
            layoutDecorated(view, bounds.left, bounds.top - verticalScrollOffset, bounds.right, bounds.bottom - verticalScrollOffset);
        }
        else {
            layoutDecorated(view, bounds.left - horizontalScrollOffset, bounds.top - verticalScrollOffset,
                    bounds.right - horizontalScrollOffset, bounds.bottom - verticalScrollOffset);
        }
    }

    private View getView(int position, Recycler recycler) {
        View view = recycler.getViewForPosition(position);
        Rect bounds = getBounds(position);
        view.setTag(R.id.table_cell_tag_data_position, position);
        view.setTag(R.id.table_cell_tag_bounds, bounds);
        measure(view, bounds);
        return view;
    }

    private void measure(View view, Rect rect) {
        if (view.getMeasuredWidth() != rect.width() || view.getMeasuredHeight() != rect.height()) {
            view.measure(MeasureSpec.makeMeasureSpec(rect.width(), MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(rect.height(), MeasureSpec.EXACTLY));
        }
    }

    private Rect getBounds(int position) {
        int y = getGrid().getColumnNo(position);
        int x = getGrid().getRowNo(position);
        Rect rect = new Rect(columnHorizontalOffsets[x], rowVerticalOffsets[y], columnHorizontalOffsets[x + 1], rowVerticalOffsets[y + 1]);
        if (isLeftToRight()) {
            return rect;
        }
        if (isRowHeader(position)) {
            return new Rect(getWidth() - rect.width(), rect.top, getWidth(), rect.bottom);
        }
        x = (columnHorizontalOffsets[columnHorizontalOffsets.length - 1] - rect.left) - rect.width();
        return new Rect(x, rect.top, rect.width() + x, rect.bottom);
    }

    private Grid getGrid() {
        if (adapter == null) {
            return null;
        }
        return adapter.getGrid();
    }

    private void recycleFirstColumn(Recycler recycler, int column) {
        if (getGrid() == null) {
            return;
        }
        if (column != scrollableFirstVisibleColumn) {
            Object obj = column > scrollableFirstVisibleColumn ? 1 : null;
            int firstVisibleColumn = obj != null ? scrollableFirstVisibleColumn : column;
            while (true) {
                if (firstVisibleColumn < (obj != null ? column : scrollableFirstVisibleColumn)) {
                    for (int i3 = scrollableFirstVisibleRow; i3 <= scrollableLastVisibleRow; i3++) {
                        int c = (getGrid().getColumnCount() * i3) + firstVisibleColumn;
                        if (obj != null) {
                            try {
                                removeAndRecycleView(children.remove(c), recycler);
                            }
                            catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        else {
                            View view = getView(c, recycler);
                            children.put(c, view);
                            addView(c, view);
                        }
                    }
                    if (isColumnHeadersEnabled()) {
                        if (obj != null) {
                            try {
                                removeAndRecycleView(children.remove(firstVisibleColumn), recycler);
                            }
                            catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        else {
                            View view = getView(firstVisibleColumn, recycler);
                            children.put(firstVisibleColumn, view);
                            addView(firstVisibleColumn, view);
                        }
                    }
                    firstVisibleColumn++;
                }
                else {
                    return;
                }
            }
        }
    }

    private void recycleFirstRow(Recycler recycler, int row) {
        if (getGrid() == null) {
            return;
        }
        if (row != scrollableFirstVisibleRow) {
            Object obj = row > scrollableFirstVisibleRow ? 1 : null;
            int actualPosition = obj != null ? scrollableFirstVisibleRow : row;
            while (true) {
                if (actualPosition < (obj != null ? row : scrollableFirstVisibleRow)) {
                    int pos;
                    for (int i = scrollableFirstVisibleColumn; i <= scrollableLastVisibleColumn; i++) {
                        pos = (getGrid().getColumnCount() * actualPosition) + i;
                        if (obj != null) {
                            try {
                                removeAndRecycleView(children.remove(pos), recycler);
                            }
                            catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        else {
                            View view = getView(pos, recycler);
                            children.put(pos, view);
                            addView(pos, view);
                        }
                    }
                    if (isRowHeadersEnabled()) {
                        pos = getGrid().getColumnCount() * actualPosition;
                        if (obj != null) {
                            try {
                                removeAndRecycleView(children.remove(pos), recycler);
                            }
                            catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        else {
                            View view = getView(pos, recycler);
                            children.put(pos, view);
                            addView(pos, view);
                        }
                    }
                    actualPosition++;
                }
                else {
                    return;
                }
            }
        }
    }

    private void recycleLastColumn(Recycler recycler, int position) {
        if (getGrid() == null) {
            return;
        }
        if (position != scrollableLastVisibleColumn) {
            Object obj = position < scrollableLastVisibleColumn ? 1 : null;
            int actualPosition = obj != null ? scrollableLastVisibleColumn : position;
            while (true) {
                if (actualPosition > (obj != null ? position : scrollableLastVisibleColumn)) {
                    for (int i = scrollableFirstVisibleRow; i <= scrollableLastVisibleRow; i++) {
                        int pos = (getGrid().getColumnCount() * i) + actualPosition;
                        if (obj != null) {
                            removeAndRecycleView(children.remove(pos), recycler);
                        }
                        else {
                            View view = getView(pos, recycler);
                            children.put(pos, view);
                            addView(pos, view);
                        }
                    }
                    if (isColumnHeadersEnabled()) {
                        if (obj != null) {
                            removeAndRecycleView(children.remove(actualPosition), recycler);
                        }
                        else {
                            View view = getView(actualPosition, recycler);
                            children.put(actualPosition, view);
                            addView(actualPosition, view);
                        }
                    }
                    actualPosition--;
                }
                else {
                    return;
                }
            }
        }
    }

    private void recycleLastRow(Recycler recycler, int row) {
        if (getGrid() == null) {
            return;
        }
        if (row != scrollableFirstVisibleRow) {
            int lastVisibleRow = row < scrollableLastVisibleRow ? scrollableLastVisibleRow : row;
            while (true) {
                if (lastVisibleRow > (row < scrollableLastVisibleRow ? row : scrollableLastVisibleRow)) {
                    int position;
                    for (int i = scrollableFirstVisibleColumn; i <= scrollableLastVisibleColumn; i++) {
                        position = (getGrid().getColumnCount() * lastVisibleRow) + i;
                        if (row < scrollableLastVisibleRow) {
                            removeAndRecycleView(children.remove(position), recycler);
                        }
                        else {
                            View view = getView(position, recycler);
                            children.put(position, view);
                            addView(position, view);
                        }
                    }
                    if (isRowHeadersEnabled()) {
                        position = getGrid().getColumnCount() * lastVisibleRow;
                        if (row < scrollableLastVisibleRow) {
                            removeAndRecycleView(children.remove(position), recycler);
                        }
                        else {
                            View view = getView(position, recycler);
                            children.put(position, view);
                            addView(position, view);
                        }
                    }
                    lastVisibleRow--;
                }
                else {
                    return;
                }
            }
        }
    }

    private boolean isRowHeader(int position) {
        return isRowHeadersEnabled() && getGrid() != null && getGrid().getRowNo(position) == 0;
    }

    private boolean isColumnHeader(int position) {
        return isColumnHeadersEnabled() && getGrid() != null && getGrid().getColumnNo(position) == 0;
    }

    private int getFirstWidth() {
        return getGrid() == null ? 0 : columnHorizontalOffsets[1];
    }

    private void updateHorizontalScrollOffset(int position) {
        int width = columnHorizontalOffsets[columnHorizontalOffsets.length - 1] - getWidth();
        Rect bounds = getBounds(position);
        horizontalScrollOffset = Math.min(isLeftToRight() ? bounds.right - getWidth() : bounds.left, width);
        if (horizontalScrollOffset < 0) {
            horizontalScrollOffset = 0;
        }
    }

    public void scrollToRow(int row) {
        int verticalOffset = rowVerticalOffsets[isColumnHeadersEnabled ? row : (row - 1)]
                - (isColumnHeadersEnabled ? getColumnHeaderHeight() : 0);
        horizontalScrollOffset = 0;
        verticalScrollOffset = verticalOffset;
        if (verticalScrollOffset < 0) {
            verticalScrollOffset = 0;
        }
        isInitialPass = true;
        requestLayout();
    }

    private void updateHorizontalOffset(int someHorizontalOffset) {
        horizontalScrollOffset = Math.min(someHorizontalOffset, columnHorizontalOffsets[columnHorizontalOffsets.length - 1] - getWidth());
        if (horizontalScrollOffset < 0) {
            horizontalScrollOffset = 0;
        }
    }

    private int getColumnHeaderHeight() {
        return getGrid() == null ? 0 : getGrid().getRow(0).getHeight();
    }

    void scrollToView(int i) {
        if (columnHorizontalOffsets == null || isInitialPass) {
            firstVisiblePosition = i;
            return;
        }
        isInitialPass = true;
        updateHorizontalScrollOffset(i);
        requestLayout();
    }

    void clipView(View view, Canvas canvas) {
        int position = (Integer) view.getTag(R.id.table_cell_tag_data_position);
        if (isRowHeader(position) && isColumnHeader(position)) {
            // top left cell
            return;
        }
        if (isRowHeader(position)) {
            canvas.clipRect(
                    rowHeaderRect.left,
                    rowHeaderRect.top - verticalScrollOffset,
                    rowHeaderRect.right,
                    rowHeaderRect.bottom - verticalScrollOffset);
        }
        else if (isColumnHeader(position)) {
            canvas.clipRect(
                    columnHeaderRect.left - horizontalScrollOffset,
                    columnHeaderRect.top,
                    columnHeaderRect.right - horizontalScrollOffset,
                    columnHeaderRect.bottom);
        }
        else {
            canvas.clipRect(
                    normalCellRect.left - horizontalScrollOffset,
                    normalCellRect.top - verticalScrollOffset,
                    normalCellRect.right - horizontalScrollOffset,
                    normalCellRect.bottom - verticalScrollOffset);
        }
    }

    public boolean isRowHeadersEnabled() {
        return isRowHeadersEnabled;
    }

    public void setRowHeadersEnabled(boolean enabled) {
        isRowHeadersEnabled = enabled;
    }

    public boolean isColumnHeadersEnabled() {
        return isColumnHeadersEnabled;
    }

    public void setColumnHeadersEnabled(boolean enabled) {
        isColumnHeadersEnabled = enabled;
    }

    public boolean isLeftToRight() {
        return isLeftToRight;
    }

    public void setLeftToRight(boolean isLeftToRight) {
        this.isLeftToRight = isLeftToRight;
    }

    @Override
    public boolean canScrollHorizontally() {
        return columnHorizontalOffsets != null && columnHorizontalOffsets[columnHorizontalOffsets.length - 1] > getWidth();
    }

    @Override
    public boolean canScrollVertically() {
        return rowVerticalOffsets != null && rowVerticalOffsets[rowVerticalOffsets.length - 1] > getHeight();
    }

    @Override
    public int computeHorizontalScrollExtent(State state) {
        return getWidth();
    }

    @Override
    public int computeHorizontalScrollOffset(State state) {
        return horizontalScrollOffset;
    }

    @Override
    public int computeHorizontalScrollRange(State state) {
        return Math.max(getWidth(), columnHorizontalOffsets == null ? 0 : columnHorizontalOffsets[columnHorizontalOffsets.length - 1]);
    }

    @Override
    public int computeVerticalScrollExtent(State state) {
        return getHeight();
    }

    @Override
    public int computeVerticalScrollOffset(State state) {
        return verticalScrollOffset;
    }

    @Override
    public int computeVerticalScrollRange(State state) {
        return Math.max(getHeight(), rowVerticalOffsets == null ? 0 : rowVerticalOffsets[rowVerticalOffsets.length - 1]);
    }

    @Override
    public void onAdapterChanged(Adapter adapter, Adapter adapter2) {
        if (adapter2 != null) {
            this.adapter = (TableAdapter) adapter2;
            //getGrid() = ((TableAdapterOld) adapter2).getGrid();
            //getGrid() = ((TableAdapterNew) adapter2).getGrid();
        }
        //else {
        //getGrid() = null;
        //}
        isInitialPass = true;
        if (adapter != null /*|| getGrid() == null*/) {
            verticalScrollOffset = 0;
            horizontalScrollOffset = 0;
        }
        removeAllViews();
    }

    @Override
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            horizontalScrollOffset = bundle.getInt("horizontalScrollOffset");
            verticalScrollOffset = bundle.getInt("verticalScrollOffset");
            super.onRestoreInstanceState(bundle.getParcelable("instanceState"));
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("instanceState", super.onSaveInstanceState());
        bundle.putInt("verticalScrollOffset", verticalScrollOffset);
        bundle.putInt("horizontalScrollOffset", horizontalScrollOffset);
        return bundle;
    }

    @Override
    public int scrollHorizontallyBy(int dx, Recycler recycler, State state) {
        int width = columnHorizontalOffsets[columnHorizontalOffsets.length - 1] - getWidth();
        if ((horizontalScrollOffset == 0 && dx <= 0) || (horizontalScrollOffset == width && dx >= 0)) {
            return 0;
        }
        if (horizontalScrollOffset + dx < 0) {
            dx = horizontalScrollOffset;
            horizontalScrollOffset = 0;
        }
        else if (horizontalScrollOffset + dx > width) {
            dx = width - horizontalScrollOffset;
            horizontalScrollOffset = width;
        }
        else {
            horizontalScrollOffset += dx;
        }
        setUpBounds();
        addAllViews(recycler);
        layoutAllViews();
        return dx;
    }

    @Override
    public int scrollVerticallyBy(int dy, Recycler recycler, State state) {
        int max = Math.max(0, rowVerticalOffsets[rowVerticalOffsets.length - 1] - getHeight());
        if ((verticalScrollOffset == 0 && dy <= 0) || (verticalScrollOffset == max && dy >= 0)) {
            return 0;
        }
        if (verticalScrollOffset + dy < 0) {
            dy = verticalScrollOffset;
            verticalScrollOffset = 0;
        }
        else if (verticalScrollOffset + dy > max) {
            dy = max - verticalScrollOffset;
            verticalScrollOffset = max;
        }
        else {
            verticalScrollOffset += dy;
        }
        setUpBounds();
        addAllViews(recycler);
        layoutAllViews();
        return dy;
    }

    private void setColumnAndRowDimens(Context context, Grid<?> grid, int[] columnHorizontalOffsets, int[] rowVerticalOffsets, int width, int height) {
        columnHorizontalOffsets[0] = 0;
        rowVerticalOffsets[0] = 0;
        for (int i = 0; i < grid.getRowCount(); i++) {
            rowVerticalOffsets[i + 1] = rowVerticalOffsets[i] + grid.getRow(i).getHeight();
        }
        int columnNo = 0;
        while (columnNo < grid.getColumnCount()) {
            Column column = grid.getColumnHeader(columnNo);
            columnHorizontalOffsets[columnNo + 1] = Math.max(column.getWidth(), column.getColumnWidth()) + columnHorizontalOffsets[columnNo];
            columnNo++;
        }
        if (width > columnHorizontalOffsets[columnHorizontalOffsets.length - 1]) {
            setColumnViewWidthsEqual(context, grid, columnHorizontalOffsets, width);
        }
        else {
            setColumnViewWidths(grid, columnHorizontalOffsets);
        }
    }

    private void setColumnViewWidthsEqual(Context context, Grid<?> grid, int[] widths, int width) {
        LinearLayout linearLayout = new LinearLayout(context);
        for (int columnNo = 0; columnNo < grid.getColumnCount(); columnNo++) {
            Column column = grid.getColumnHeader(columnNo);
            View view = new View(context);
            view.setMinimumWidth(column.getWidth());
            view.setTag(column.getWidth());
            linearLayout.addView(view, new LinearLayout.LayoutParams(column.getColumnWidth(), 1, column.getWeight()));
        }
        linearLayout.measure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(1, MeasureSpec.EXACTLY));
        linearLayout.layout(0, 0, width, 1);
        int columnNo = 0;
        while (columnNo < grid.getColumnCount()) {
            View view = linearLayout.getChildAt(columnNo);
            Column column = grid.getColumnHeader(columnNo);
            int viewTagWidth = (Integer) view.getTag();
            if (view.getWidth() < viewTagWidth) {
                column.setColumnWidth(viewTagWidth);
                setColumnViewWidthsEqual(context, grid, widths, viewTagWidth);
                return;
            }
            viewTagWidth = view.getWidth();
            column.setViewWidth(viewTagWidth);
            widths[columnNo + 1] = viewTagWidth + widths[columnNo];
            columnNo++;
        }
    }

    private void setColumnViewWidths(Grid<?> grid, int[] widths) {
        for (int i = 0; i < grid.getColumnCount(); i++) {
            Column column = grid.getColumnHeader(i);
            int columnWidth = column.getColumnWidth();
            if (columnWidth == 0) {
                columnWidth = column.getWidth();
            }
            column.setViewWidth(columnWidth);
            widths[i + 1] = columnWidth + widths[i];
        }
    }

    private int indexOfKey(int[] a, int key) {
        int index = Arrays.binarySearch(a, key);
        return index < 0 ? Math.abs(index + 2) : index - 1;
    }
}
