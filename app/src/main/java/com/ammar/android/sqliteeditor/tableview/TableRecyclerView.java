package com.ammar.android.sqliteeditor.tableview;

import android.content.Context;
import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import com.ammar.android.sqliteeditor.R;

public class TableRecyclerView extends RecyclerView {

    private final TableLayoutManager tableLayoutManager;
    private       ItemDecorator      tableDecorator;
    private       TableAdapter       tableAdapter;

    public interface ItemDecorator {

        void drawDividersAndShadows(Canvas canvas, TableRecyclerView tableRecyclerView);

        void m11308b(Canvas canvas, TableRecyclerView tableRecyclerView);
    }

    public TableRecyclerView(Context context) {
        this(context, null);
    }

    public TableRecyclerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        if (isInEditMode()) {
            this.tableLayoutManager = null;
            return;
        }
        this.tableLayoutManager = new TableLayoutManager(getContext());
        this.tableLayoutManager.setLeftToRight(getResources().getBoolean(R.bool.table_is_left_to_right));
        setLayoutManager(this.tableLayoutManager);
        setHasFixedSize(true);
    }

    public void scrollToView(int i) {
        this.tableLayoutManager.scrollToView(i);
    }

    public boolean isRowHeadersEnabled() {
        return this.tableLayoutManager.isRowHeadersEnabled();
    }

    public boolean isColumnHeadersEnabled() {
        return this.tableLayoutManager.isColumnHeadersEnabled();
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        if (tableDecorator != null && !tableLayoutManager.isInitialPass) {
            this.tableDecorator.m11308b(canvas, this);
        }
        super.dispatchDraw(canvas);
        if (tableDecorator != null && !tableLayoutManager.isInitialPass) {
            this.tableDecorator.drawDividersAndShadows(canvas, this);
        }
    }

    @Override
    public boolean drawChild(Canvas canvas, View view, long drawingTime) {
        if (tableAdapter != null) {
            canvas.save();
            tableLayoutManager.clipView(view, canvas);
        }
        boolean childDrawn = super.drawChild(canvas, view, drawingTime);
        if (tableAdapter != null) {
            canvas.restore();
        }
        return childDrawn;
    }

    @Deprecated
    @Override
    public Adapter getAdapter() {
        return super.getAdapter();
    }

    public int[] getHeights() {
        return this.tableLayoutManager.rowVerticalOffsets;
    }

    public int getHorizontalScrollOffset() {
        return this.tableLayoutManager.horizontalScrollOffset;
    }

    public int getScrollableFirstVisibleColumn() {
        return this.tableLayoutManager.scrollableFirstVisibleColumn;
    }

    public int getScrollableFirstVisibleRow() {
        return this.tableLayoutManager.scrollableFirstVisibleRow;
    }

    public int getScrollableLastVisibleColumn() {
        return this.tableLayoutManager.scrollableLastVisibleColumn;
    }

    public int getScrollableLastVisibleRow() {
        return this.tableLayoutManager.scrollableLastVisibleRow;
    }

    public TableAdapter getTableAdapter() {
        return this.tableAdapter;
    }

    public ItemDecorator getTableDecorator() {
        return this.tableDecorator;
    }

    public int getVerticalScrollOffset() {
        return this.tableLayoutManager.verticalScrollOffset;
    }

    public int[] getWidths() {
        return this.tableLayoutManager.columnHorizontalOffsets;
    }

    @Deprecated
    @Override
    public void setAdapter(Adapter adapter) {
        super.setAdapter(adapter);
    }

    @Deprecated
    public <T> void setAdapter(TableAdapter tableAdapter) {
        setTableAdapter(tableAdapter);
    }

    public void setHasStaticFirstColumn(boolean z) {
        this.tableLayoutManager.setRowHeadersEnabled(z);
    }

    public void setHasStaticHeader(boolean z) {
        this.tableLayoutManager.setColumnHeadersEnabled(z);
    }

    public void setLeftToRight(boolean z) {
        this.tableLayoutManager.setLeftToRight(z);
    }

    public void setTableAdapter(TableAdapter tableAdapter) {
        this.tableAdapter = tableAdapter;
        super.setAdapter(tableAdapter);
    }

    public void setTableDecorator(ItemDecorator itemDecorator) {
        this.tableDecorator = itemDecorator;
    }

    public TableLayoutManager getTableLayoutManager() {
        return this.tableLayoutManager;
    }

    public void scrollToRow(int rowId) {
        getTableLayoutManager().scrollToRow(rowId);
    }
}
