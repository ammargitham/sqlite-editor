package com.ammar.android.sqliteeditor.utils

import android.util.Log
import android.widget.EditText
import com.ammar.android.sqliteeditor.getTextWatcherObservable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.subjects.PublishSubject
import java.util.*

/**
 * Created by agitham on 9/6/2017.
 */
internal class MultiSourceCombinatorOld {

    /**
     * We can't handle arbitrary number of sources by CombineLatest, but we can pass data along
     * with information about source (sourceId)
     */
    private class SourceData {

        internal var data = ""
        internal var sourceId: Int = 0
    }

    /**
     * Keep id of source, subscription to that source and last value emitted
     * by source. This value is passed when source is attached
     */
    private inner class SourceInfo internal constructor(internal var sourceId: Int, data: String) {

        internal var sourceTracking: Disposable? = null
        internal var lastData: SourceData

        init {
            // initialize last data with empty value
            val d = SourceData()
            d.data = data
            d.sourceId = sourceId
            this.lastData = d
        }
    }

    /**
     * We can tunnel data from all sources into single pipe. Subscriber can treat it as
     * Observable<SourceData>
    </SourceData> */
    private val dataDrain: PublishSubject<SourceData>
    /**
     * Stores all sources by their ids.
     */
    private var sources: MutableMap<Int, SourceInfo>
    /**
     * Callback, notified whenever source emit new data. it receives source id.
     * When notification is received by client, it can get value from source by using
     * getLastSourceValue(sourceId) method
     */
    internal var sourceUpdateCallback: Consumer<Int>? = null

    init {
        dataDrain = PublishSubject.create<SourceData>()
        sources = HashMap<Int, SourceInfo>()
        sourceUpdateCallback = null
        // We have to process data, ccoming from common pipe
        dataDrain.subscribe { newValue ->
            if (sourceUpdateCallback == null) {
                Log.w(TAG, "Source " + newValue.sourceId + "emitted new value, but user did't set callback ")
            }
            else {
                sourceUpdateCallback?.accept(newValue.sourceId)
            }
        }
    }

    /**
     * Disconnect from all sources (sever Connection (s))
     */
    fun stop() {
        Log.i(TAG, "Unsubscribing from all sources")
        // copy references to avoid ConcurrentModificatioinException
        val t = ArrayList(sources.values)
        for (si in t) {
            removeSource(si.sourceId)
        }
        // right now there must be no active sources
        if (!sources.isEmpty()) {
            throw RuntimeException("There must be no active sources")
        }
    }

    /**
     * Create new source from edit field, subscribe to this source and save subscription for
     * further tracking.

     * @param editText
     */
    fun addSource(editText: EditText, sourceId: Int) {
        if (sources.containsKey(sourceId)) {
            Log.e(TAG, "Source with id $sourceId already exist")
            return
        }
        val source = editText.getTextWatcherObservable().skip(1)
        val lastValue = editText.text.toString()
        Log.i(TAG, "Source with id $sourceId has data $lastValue")
        // Redirect data coming from source to common pipe, to do that attach source id to
        // data string
        val sourceSubscription = source.subscribe { text ->
            val s = text.toString()
            val nextValue = SourceData()
            nextValue.sourceId = sourceId
            nextValue.data = s
            Log.i(TAG, "Source " + sourceId + "emits new value: " + s)
            // save vlast value
            sources[sourceId]?.lastData?.data = s
            // pass new value down pipeline
            dataDrain.onNext(nextValue)
        }
        //sourceSubscription ?: throw IllegalArgumentException("sourceSubscription for $sourceId is null")
        // create SourceInfo
        val sourceInfo = SourceInfo(sourceId, lastValue)
        sourceInfo.sourceTracking = sourceSubscription
        sources.put(sourceId, sourceInfo)
    }

    /**
     * Unsubscribe source from common pipe and remove it from list of sources

     * @param sourceId
     * *
     * @throws IllegalArgumentException
     */
    @Throws(IllegalArgumentException::class)
    fun removeSource(sourceId: Int) {
        if (!sources.containsKey(sourceId)) {
            throw IllegalArgumentException("There is no source with id: " + sourceId)
        }
        val si = sources[sourceId]
        val s = si?.sourceTracking
        if (null != s && !s.isDisposed) {
            Log.i(TAG, "source $sourceId is active, unsubscribing from it")
            si.sourceTracking?.dispose()
            si.sourceTracking = null
        }
        // source is disabled, remove it from list
        Log.i(TAG, "Source $sourceId is disabled ")
        sources.remove(sourceId)
    }

    /**
     * User can get value from any source by using source ID.

     * @param sourceId
     * *
     * @return
     * *
     * @throws IllegalArgumentException
     */
    @Throws(IllegalArgumentException::class)
    fun getLastSourceValue(sourceId: Int): String {
        if (!sources.containsKey(sourceId)) {
            throw IllegalArgumentException("There is no source with id: " + sourceId)
        }
        val lastValue = sources[sourceId]?.lastData?.data
        return lastValue ?: ""
    }

    fun setSourceUpdateCallback(sourceUpdateFeedback: Consumer<Int>) {
        this.sourceUpdateCallback = sourceUpdateFeedback
    }

    companion object {

        private val TAG = MultiSourceCombinatorOld::class.java.simpleName
    }
}
