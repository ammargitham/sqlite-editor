package com.ammar.android.sqliteeditor.utils

import android.util.SparseBooleanArray

import java.io.IOException
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.io.Serializable

/**
 * @author Asaf Pinhassi www.mobiledev.co.il
 */
internal class SerializableSparseBooleanArray : SparseBooleanArray, Serializable {

    constructor(capacity: Int) : super(capacity)

    constructor() : super()

    /**
     * This method is private but it is called using reflection by java
     * serialization mechanism. It overwrites the default object serialization.
     *
     *
     * <br></br><br></br>**IMPORTANT**
     * The access modifier for this method MUST be set to **private** otherwise [java.io.StreamCorruptedException]
     * will be thrown.

     * @param oos the stream the data is stored into
     * *
     * @throws IOException an exception that might occur during data storing
     */
    @Throws(IOException::class)
    private fun writeObject(oos: ObjectOutputStream) {
        val data = arrayOfNulls<Any>(size())
        for (i in data.indices) {
            val pair = arrayOf(keyAt(i), valueAt(i))
            data[i] = pair
        }
        oos.writeObject(data)
    }

    /**
     * This method is private but it is called using reflection by java
     * serialization mechanism. It overwrites the default object serialization.
     *
     *
     * <br></br><br></br>**IMPORTANT**
     * The access modifier for this method MUST be set to **private** otherwise [java.io.StreamCorruptedException]
     * will be thrown.

     * @param ois the stream the data is read from
     * *
     * @throws IOException            an exception that might occur during data reading
     * *
     * @throws ClassNotFoundException this exception will be raised when a class is read that is
     * *                                not known to the current ClassLoader
     */
    @Throws(IOException::class, ClassNotFoundException::class)
    private fun readObject(ois: ObjectInputStream) {
        val data = ois.readObject() as Array<*>
        data.map { it as Array<*> }
                .forEach { this.append(it[0] as Int, it[1] as Boolean) }
    }

    companion object {
        private const val serialVersionUID = 824056059663678000L
    }
}