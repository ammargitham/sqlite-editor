package com.ammar.android.sqliteeditor.utils

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import java.util.*

/**
 * Created by Ammar Githam on 26/04/2017.
 */
internal object SqliteHelper {

    private val TAG = "MyGrid"

    fun getTableNames(db: SQLiteDatabase): List<String> {
        val cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null)
        val tableNames = ArrayList<String>()
        while (cursor.moveToNext()) {
            //Log.d(TAG, "getTableNames: " + cursor.getHeaderText(0));
            tableNames.add(cursor.getString(0))
        }
        cursor.close()
        return tableNames
    }

    fun getDataString(cursor: Cursor, colNo: Int): String? {
        val type = cursor.getType(colNo)
        when (type) {
            Cursor.FIELD_TYPE_STRING -> return cursor.getString(colNo)
            Cursor.FIELD_TYPE_FLOAT -> return cursor.getFloat(colNo).toString()
            Cursor.FIELD_TYPE_INTEGER -> return cursor.getInt(colNo).toString()
            Cursor.FIELD_TYPE_NULL -> return null
            Cursor.FIELD_TYPE_BLOB -> return "...."
        }
        return null
    }
}
