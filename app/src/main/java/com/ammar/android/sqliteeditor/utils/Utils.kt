package com.ammar.android.sqliteeditor.utils

import android.content.Context
import android.util.TypedValue

internal object Utils {

    private val TAG = "Utils"

    fun dp2px(context: Context, dp: Float) = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.resources.displayMetrics)

    fun isNumeric(s: String?) = s != null && s.matches("[-+]?\\d*\\.?\\d+".toRegex())

    fun containsIgnoreCase(src: String?, what: String?): Boolean {
        if (src == null || what == null) {
            return false
        }
        val length = what.length
        if (length == 0) {
            return true // Empty string is contained
        }
        val firstLo = Character.toLowerCase(what[0])
        val firstUp = Character.toUpperCase(what[0])
        for (i in src.length - length downTo 0) {
            // Quick check before calling the more expensive regionMatches() method:
            val ch = src[i]
            if (ch != firstLo && ch != firstUp) {
                continue
            }
            if (src.regionMatches(i, what, 0, length, ignoreCase = true)) {
                return true
            }
        }
        return false
    }
}
